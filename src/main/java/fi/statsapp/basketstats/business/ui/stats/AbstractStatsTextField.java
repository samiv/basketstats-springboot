package fi.statsapp.basketstats.business.ui.stats;

import java.util.Optional;

import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public abstract class AbstractStatsTextField extends JTextField {

	private static final long serialVersionUID = 1L;

	protected JCheckBox checkBox;
	
	protected JScrollPane optionsScrollPanel;	
	protected AbstractStatsTextField previousField;
	protected AbstractStatsTextField nextField;
	
	protected String labelText;
	
	public AbstractStatsTextField() {
		initTextFieldField();
		initCheckBox();
	}
	
	// Different implementations for PositionTextsField and StatsTextField
	public abstract void initCheckBox();

	// Different implementations for PositionTextsField and StatsTextField
	public abstract void initTextFieldField();
	
	public AbstractStatsTextField getPreviousField() {
		return previousField;
	}

	public AbstractStatsTextField getNextField() {
		return nextField;
	}
	
	public void setNextField(AbstractStatsTextField nextField) {
		this.nextField = nextField;
	}

	public void setPreviousAndNextField(AbstractStatsTextField previousField, AbstractStatsTextField nextField) {
		this.nextField = nextField;
		this.previousField = previousField;
	}
	
	public String getLabelText() {
		return labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
		setText(labelText);
	}
	
	public JCheckBox getCheckBox() {
		return checkBox;
	}	

	public JScrollPane getScrollPanel() {
		return Optional.ofNullable(optionsScrollPanel).orElse(null);
	}

	public void setScrollPanel(JScrollPane scrollPanel) {
		this.optionsScrollPanel = scrollPanel;
	}
	
	public AbstractStatsTextField findNextSelected() {
		AbstractStatsTextField nextEnabled = nextField;
		// TODO: RISKI!!! Voi jäädä jumiin
		while(true) {
			if (nextEnabled == null) {
				return null;
			}

			if (!nextEnabled.getCheckBox().isSelected()) {
				nextEnabled = nextEnabled.getNextField();
				continue;
			}

			return nextEnabled;
		}
	}

	public AbstractStatsTextField findPreviousSelected() {
		AbstractStatsTextField previousEnabled = previousField;
		// TODO: RISKI!!! Voi jäädä jumiin
		while(true) {
			if (previousEnabled == null) {
				return previousEnabled;
			}
			
			if(!previousEnabled.getCheckBox().isSelected()) {
				previousEnabled = previousEnabled.previousField;
				continue;
			}
			
			return previousEnabled;
		}
	}
}
