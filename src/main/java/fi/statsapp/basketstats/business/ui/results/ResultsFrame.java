package fi.statsapp.basketstats.business.ui.results;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.AbstractAppFrame;
import fi.statsapp.basketstats.business.entity.ScreenEvent;
import net.miginfocom.swing.MigLayout;

@Component
public class ResultsFrame extends AbstractAppFrame {
	
	private static final long serialVersionUID = 1L;
	private ResultsPanel resultsPanel;
	private JList<ScreenEvent> resultsList;
	private DefaultListModel<ScreenEvent> resultsListModel;

	private static final String PANEL_TITLE_LBL = "SEARCH RESULTS";
	
	@Autowired
	public ResultsFrame(ResultsPanel resultsPanel) {
		this.resultsPanel = resultsPanel;
		initialize();
		initComponents();
	}

	private void initComponents() {

		add(new JLabel(PANEL_TITLE_LBL), "cell 0 0, pushx, alignx center");
		resultsListModel = new DefaultListModel<ScreenEvent>();
		resultsList = new JList<ScreenEvent>();
		resultsList.setModel(resultsListModel);
		resultsList.setCellRenderer(new ResultsListCellRenderer());
		add(resultsPanel, "cell 0 1");
		add(new JScrollPane(resultsList), "cell 1 1, grow, push");
	}

	@Override
	public void initialize() {
		setTitle("Results");
//		add(resultsPanel);
		
		setLayout(new MigLayout("insets 20 30 40 40", "[]20[grow]", "[]30[grow]"));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(getAppSizeX(), getAppSizeY());
        setLocationRelativeTo(null);
        setResizable(false);
		
	}
	
	public ResultsPanel getResultsPanel() {
		return resultsPanel;
	}
	
	public void addResultsToList(List<ScreenEvent> a) {
		resultsListModel.clear();
		for (ScreenEvent screenEvent : a) {
			resultsListModel.addElement(screenEvent);
		}
	}

}
