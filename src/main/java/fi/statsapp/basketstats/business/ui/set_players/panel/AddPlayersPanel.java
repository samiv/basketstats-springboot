package fi.statsapp.basketstats.business.ui.set_players.panel;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.springframework.stereotype.Component;

import net.miginfocom.swing.MigLayout;

@Component
public class AddPlayersPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private static final String ADD_PLAYER_TEXT = "Add player:";
	private static final String NUMBER_TEXT = "#";
	private static final String NAME_TEXT = "Player name";
	private static final String ADD_TEXT = "Add";
	
	private JButton addButton;
	private JTextField jerseyTextField;
	private JTextField nameTextField;	


	public AddPlayersPanel() {
		JLabel nameLabel = new JLabel(NAME_TEXT);
		JLabel numberLabel = new JLabel(NUMBER_TEXT);
		JLabel addPlayerLabel = new JLabel(ADD_PLAYER_TEXT);
		jerseyTextField = new JTextField();
		nameTextField = new JTextField();
		this.addButton = new JButton(ADD_TEXT);
//		this.addButton.addActionListener(this);
		setLayout(new MigLayout("", "[60px][140px][40px][][grow]", "[14px][14px]"));		
		add(nameLabel, "cell 1 0,alignx center,aligny top");
		add(numberLabel, "cell 2 0,alignx center,aligny top");
		add(addButton, "cell 3 1");
		add(jerseyTextField, "cell 2 1, growx, growy");
		add(nameTextField, "cell 1 1, growx, growy");
		add(addPlayerLabel, "cell 0 1,alignx left,aligny center");
	}
	
	public JButton getAddButton() {
		return addButton;
	}
	
	public JTextField getNameTextField() {
		return nameTextField;
	}
	
	public JTextField getJerseyTextField() {
		return jerseyTextField;
	}
}
