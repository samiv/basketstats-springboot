package fi.statsapp.basketstats.business.ui.set_players;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.player.Team;
import fi.statsapp.basketstats.business.player.TeamStore;
import fi.statsapp.basketstats.business.ui.set_players.panel.AddPlayersPanel;
import fi.statsapp.basketstats.business.ui.shared.controller.AbstractFrameController;

@Controller
public class SetPlayersController extends AbstractFrameController {
	
	private static final Logger LOG = LoggerFactory.getLogger(SetPlayersController.class);

	private SetPlayersFrame setPlayerFrame;
	// TODO: mitäs tällä playerSetillä oikeen tekee -> Käytetään tuolla removeButtonin iteroinnissa. Voisko tehdä ilman tätä settiä?
	private Set<Player> playerSet = new TreeSet<Player>();
	private boolean isPlayerListChanged = false;
	private boolean isAllSelected = false;
	private String ownTeamString = "";

	@Autowired
	public SetPlayersController(SetPlayersFrame setPlayerFrame) {
		this.setPlayerFrame = setPlayerFrame;
	}
	
	@Override
	public void prepareAndOpenFrame() {
		openSetPlayersFrame();

	}

	public void prepareAndOpenFrame(Team ownTeam) {
		
		ownTeamString = ownTeam.getTeamName();
		setPlayerFrame.getPlayerListPanel().getPlayerListModel().removeAllElements();
		playerSet.addAll(ownTeam.getPlayerList());
		
		for(Player player : ownTeam.getPlayerList()) {
			setPlayerFrame.getPlayerListPanel().getPlayerListModel().addElement(player);
		}
		LOG.trace("PlayerListPanel -> PlayerListModel updated. PlayerList includes values: {}", setPlayerFrame.getPlayerListPanel().getPlayerListModel());
		prepareAndOpenFrame();
	}

	public SetPlayersFrame getSetPlayerFrame() {
		return setPlayerFrame;
	}

	public void addPlayersButtonAction(AddPlayersPanel addPlayersPanel, DefaultListModel<Player> listModel, TeamStore teamStore) {
		// Mark that XML file should be updated
		isPlayerListChanged = true;
		String jerseyText = addPlayersPanel.getJerseyTextField().getText();				
		String playerName = addPlayersPanel.getNameTextField().getText();
		Integer playerJersey = Integer.parseInt(jerseyText);
		Player targetPlayer = new Player(playerName, playerJersey);

		// TODO: pitää keksiä joku juttu tähän 00-käsittelyyn
		if(jerseyText == "00") {
			listModel.add(0, targetPlayer);
		}

		this.playerSet.add(targetPlayer);
		for (int i = 0; i < listModel.size(); i++) {
			Integer jerseyItem = listModel.elementAt(i).getJersey();
			if(jerseyItem > targetPlayer.getJersey()) {
				listModel.add(i, targetPlayer);
				teamStore.findTeamByName(ownTeamString).addPlayer(targetPlayer);
				addPlayersPanel.getJerseyTextField().setText("");
				addPlayersPanel.getNameTextField().setText("");
				return;
			}
		}
		listModel.addElement(targetPlayer);
		teamStore.findTeamByName(ownTeamString).addPlayer(targetPlayer);
		addPlayersPanel.getJerseyTextField().setText("");
		addPlayersPanel.getNameTextField().setText("");
	}
	
	public void addSelectButtonAction(DefaultListModel<Player> listModel, JList<Player> playerJlist) {
		int[] indecies = playerJlist.getSelectedIndices();
		if(indecies.length > 0) {
			for(int idx : indecies) {
				listModel.get(idx).setSelected(true);
				listModel.set(idx, listModel.get(idx));
			}
		}
	}
	
	public void addModifyButtonAction() {
		isPlayerListChanged = true;
//		modifyItem();
	}
	
	public void addRemoveButtonAction(JList<Player> playerJlist, DefaultListModel<Player> listModel, TeamStore teamStore) {
		isPlayerListChanged = true;
		int iSelected = playerJlist.getSelectedIndex();
		if(iSelected == -1) {
			return;
		}
		Iterator<Player> iterator = this.playerSet.iterator();
		while(iterator.hasNext()) {
			if (iterator.next().equals(playerJlist.getSelectedValue())) {
				teamStore.findTeamByName(ownTeamString).removePlayer(playerJlist.getSelectedValue());
				iterator.remove();
				listModel.remove(iSelected);
				break;
			}
		}
	}
	
	public void addDeSelectButtonAction(JList<Player> playerJlist, DefaultListModel<Player> listModel) {
		int[] indecies = playerJlist.getSelectedIndices();
		if(indecies != null) {
			for(int idx : indecies) {
				listModel.get(idx).setSelected(false);
				listModel.set(idx, listModel.get(idx));
			}
		}
	}

	private void openSetPlayersFrame() {
		setPlayerFrame.setVisible(true);
	}

	public boolean getIsPlayerListChanged() {
		return isPlayerListChanged;
	}

	public void setIsPlayerListChanged(boolean isPlayerListChanged) {
		this.isPlayerListChanged = isPlayerListChanged;
	}

	public String getOwnTeamString() {
		return ownTeamString;
	}

	public void setOwnTeamString(String ownTeamString) {
		this.ownTeamString = ownTeamString;
	}

	public void addSwitchButtonAction(JList<Player> list, DefaultListModel<Player> listModel) {
		for(int i = 0; listModel.size() > i; ++i) {
			listModel.getElementAt(i).setSelected(!isAllSelected);
		}
		isAllSelected = !isAllSelected;
		setPlayerFrame.revalidate();
		setPlayerFrame.repaint();
	}
	
}
