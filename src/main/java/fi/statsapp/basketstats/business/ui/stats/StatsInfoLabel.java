package fi.statsapp.basketstats.business.ui.stats;

import java.awt.Color;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class StatsInfoLabel extends JLabel {
	
	private static final long serialVersionUID = 1L;
	
	private static final String FAILED_ICON_PATH = "icons\\failed.png";
	private static final String SUCCESS_ICON_PATH = "icons\\success.png";
	private static final String REMOVED_ICON_PATH = "icons\\remove.png";
	private static final String MODIFIED_ICON_PATH = "icons\\apply.png";
	
	private ImageIcon failedIcon;
	private ImageIcon successIcon;
	private ImageIcon removedIcon;
	private ImageIcon modifiedIcon;
	
	public StatsInfoLabel() {
		failedIcon = loadImage(FAILED_ICON_PATH);
		successIcon = loadImage(SUCCESS_ICON_PATH);
		removedIcon = loadImage(REMOVED_ICON_PATH);
		modifiedIcon = loadImage(MODIFIED_ICON_PATH);
		setText("");
	}
	
	public void setFailed(String message) {
		setIcon(failedIcon);
		setText(message);
		setForeground(Color.RED);
	}
	
	public void setSuccess(String message) {
		setIcon(successIcon);
		setText(message);
		setForeground(Color.GREEN);
	}
	
	public void setRemoved(String message) {
		setIcon(removedIcon);
		setText(message);
		setForeground(new Color(160, 32 ,255));
	}
	
	public void setModified(String message) {
		setIcon(modifiedIcon);
		setText(message);
		setForeground(Color.BLUE);
	}
	
	private ImageIcon loadImage(String path) {
		URL url = Thread.currentThread().getContextClassLoader().getResource(path);
		return new ImageIcon (new ImageIcon(url).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	}

}
