package fi.statsapp.basketstats.business.ui.set_players.panel;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import net.miginfocom.swing.MigLayout;

@Component
public class SelectTeamPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final String SELECT_TEAM = "Select Team:";

	private String ownTeam;
	private String teamAgainst;
	private JComboBox<String> teamComboBox;

	public SelectTeamPanel() {
		setLayout(new MigLayout());
	}
	
	public void addTeamComboBox(String ownTeam, String teamAgainst) {
		removeAll();
		JLabel selectTeamLabel = new JLabel(SELECT_TEAM);
		add(selectTeamLabel);
		this.teamComboBox = new JComboBox<String>();
		teamComboBox.addItem(ownTeam);
		teamComboBox.addItem(teamAgainst);
		add(teamComboBox);
		revalidate();
		repaint();
	}
	
	public void setHomeText(String homeTeam) {
		this.ownTeam = homeTeam;
		repaint();
	}
	
	public void setQuestText(String questTeam) {
		this.teamAgainst = questTeam;
	}
	
	public String getHomeText() {
		return ownTeam;
	}
	
	public String getQuestText() {
		return teamAgainst;
	}
	
}
