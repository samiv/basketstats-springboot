package fi.statsapp.basketstats.business.ui.stats.panel;

import java.awt.Font;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.buttons.BackToStartButton;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import net.miginfocom.swing.MigLayout;

@Component
public class MatchInfoPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private BackToStartButton backToStartButton;

	public MatchInfoPanel() {
		setLayout(new MigLayout("", "[]", "[]30px[][]"));
		backToStartButton = new BackToStartButton();
	}



	public void setGameInfo(String ownTeamName, String teamAgainstName, Date gameDate, Optional<HomeAwayEnum> homeAway) {
		removeAll();
		Format formatter = new SimpleDateFormat("dd-MM-yyyy");
		JLabel gameDateLbl = new JLabel(formatter.format(gameDate));
		gameDateLbl.setFont(new Font("Dialog", Font.BOLD, 18));
		JLabel matchTitle = new JLabel();
		
		if(!homeAway.isPresent() || homeAway.get() == HomeAwayEnum.HOME) {
			matchTitle.setText(ownTeamName + " - " + teamAgainstName);
		} else if(homeAway.get() == HomeAwayEnum.AWAY) {
			matchTitle.setText(teamAgainstName + " - " + ownTeamName);			
		}
		matchTitle.setFont(new Font("Dialog", Font.BOLD, 18));
		matchTitle.setBounds(40, 50, 220, 40);
		add(backToStartButton, "wrap, aligny top");
		add(gameDateLbl, "wrap");
		add(matchTitle, "wrap");
		homeAway.ifPresent(ha -> add(new JLabel("(" + ha.toString().charAt(0) + ha.toString().substring(1, ha.toString().length()).toLowerCase() + ")")));
		revalidate();
		repaint();
	}

	public BackToStartButton getBackToStartButton() {
		return backToStartButton;
	}

}
