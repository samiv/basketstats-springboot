package fi.statsapp.basketstats.business.ui.stats.frame;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.ui.stats.panel.ActivityPanel;
import fi.statsapp.basketstats.business.ui.stats.panel.EnterStatsPanel;
import fi.statsapp.basketstats.business.ui.stats.panel.MatchInfoPanel;
import fi.statsapp.basketstats.business.ui.stats.panel.StatsCourtPanel;
import net.miginfocom.swing.MigLayout;

@Component
public class StatsAppFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private MatchInfoPanel matchInfoPanel;
	private ActivityPanel activityPanel;
	private StatsCourtPanel statsCourtPanel;
	private EnterStatsPanel enterStatsPanel;
	// this is used for keyListeners
	private boolean isOpen = false;
	
	@Autowired
	public StatsAppFrame(MatchInfoPanel matchInfoPanel, ActivityPanel activityPanel, StatsCourtPanel statsCourtPanel, EnterStatsPanel enterStatsPanel) {
		this.matchInfoPanel = matchInfoPanel;
		this.activityPanel = activityPanel;
		this.statsCourtPanel = statsCourtPanel;
		this.enterStatsPanel = enterStatsPanel;
		initFrameAndComponents();
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new MyDispatcher());
		

		// Ei oikeestaan tarvitse asettaa koska koko ohjelma suljetaan. Varmistuskysymys tosin ihan jees.
		// TODO: Backbuttonissa pitää sitten asettaa isOpen = false;
		this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int i=JOptionPane.showConfirmDialog(null, "Do you want to close program?");
                if(i==JOptionPane.OK_OPTION) {
                	isOpen = false;
                    System.exit(0);
                }
            }
        });

	}

	private void initFrameAndComponents() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new MigLayout("","[400px][500px]","[][500px]"));
		add(matchInfoPanel, "cell 0 0, grow");
		add(activityPanel, "cell 1 0, pushx, grow");
		add(statsCourtPanel, "cell 0 1, grow");
		add(enterStatsPanel, "cell 1 1, grow");
		pack();
		setLocationRelativeTo(null);
	}

	public MatchInfoPanel getMatchInfoPanel() {
		return matchInfoPanel;
	}

	public ActivityPanel getActivityPanel() {
		return activityPanel;
	}

	public EnterStatsPanel getEnterStatsPanel() {
		return enterStatsPanel;
	}
	
	public StatsCourtPanel getStatsCourtPanel() {
		return statsCourtPanel;
	}
	
	public void updateCourtPanel(JPanel updatedCourtPanel) {
		removeAll();
		revalidate();
		repaint();
	}
	
	public void ScreenButtonAction(ScreenPosition screenPosition) {
		statsCourtPanel.setScreenButtonsEnabled(false);
		enterStatsPanel.updateScreenPositionLabelString(screenPosition.getValue());
		enterStatsPanel.setFieldsEnabled(false);
		enterStatsPanel.setAddingElement(true);
		enterStatsPanel.setCheckBoxesEnabled(false);
		
		enterStatsPanel.setFirstCheckBoxEnabledAndFocusAndOptionsPanel();
	}

	
	private class MyDispatcher implements KeyEventDispatcher {
		@Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			
			if (e.getID() == KeyEvent.KEY_RELEASED && !enterStatsPanel.isAddingElement() && isOpen) {
				if (e.getKeyCode() == KeyEvent.VK_1) {
					ScreenButtonAction(ScreenPosition.SIDE_LEFT);
				} else if (e.getKeyCode() == KeyEvent.VK_2) {
					ScreenButtonAction(ScreenPosition.ELBOW_LEFT);
				} else if (e.getKeyCode() == KeyEvent.VK_3) {
					ScreenButtonAction(ScreenPosition.MIDDLE);
				} else if (e.getKeyCode() == KeyEvent.VK_4) {
					ScreenButtonAction(ScreenPosition.ELBOW_RIGTH);
				} else if (e.getKeyCode() == KeyEvent.VK_5) {
					ScreenButtonAction(ScreenPosition.SIDE_RIGHT);
				}
			}
			return false;
		}
	}


	public boolean isOpen() {
		return isOpen;
	}

	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
}
