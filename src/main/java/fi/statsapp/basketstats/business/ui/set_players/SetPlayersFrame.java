package fi.statsapp.basketstats.business.ui.set_players;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.buttons.BackToStartButton;
import fi.statsapp.basketstats.business.ui.set_players.panel.AddPlayersPanel;
import fi.statsapp.basketstats.business.ui.set_players.panel.PlayerListPanel;
import net.miginfocom.swing.MigLayout;

@Component
public class SetPlayersFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private static final String SET_PLAYERS_VIEW_TITLE = "Set players";
	private static final String NEXT_TEXT = "Next  >>>";
	
	private JButton nextButton;
	private BackToStartButton backToStartButton;

//	private Set<Player> playerSet = new TreeSet<Player>();
	
//	private SelectTeamPanel selectTeamPanel;
	private AddPlayersPanel addPlayersPanel;
	private PlayerListPanel playersListPanel;

	@Autowired
	public SetPlayersFrame(AddPlayersPanel addPlayersPanel, PlayerListPanel playersListPanel) {
//		this.selectTeamPanel = selectTeamPanel;
		this.addPlayersPanel = addPlayersPanel;
		this.playersListPanel = playersListPanel;
		initSetPlayersView();
		initComponents();
		
	}
	
    private void initComponents() {
//        add(selectTeamPanel, "cell 0 0,alignx left,aligny top");
        add(addPlayersPanel, "cell 0 1,alignx left,aligny top");
        add(playersListPanel, "cell 0 2,grow");
        backToStartButton = new BackToStartButton();
        add(backToStartButton, "cell 0 3, gapleft 8, aligny bottom, split 2");
        add(createNextButton(), "cell 0 3, gapleft 250, aligny top");
    }
	
//	public SelectTeamPanel getSelectTeamPanel() {
//		return selectTeamPanel;
//	}
	
	public AddPlayersPanel getAddPlayersPanel() {
		return addPlayersPanel;
	}
	
	public PlayerListPanel getPlayerListPanel() {
		return playersListPanel;
	}
	

	private void initSetPlayersView(){
//		LOGGER.debug("Creating SetPlayersView");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 700);
		setResizable(false);
		setTitle(SET_PLAYERS_VIEW_TITLE);
		setLayout(new MigLayout("insets 5 5 5 30", "[317px,grow]", "[34px][55px]15px[480px]7px[40px]"));
		setLocationRelativeTo(null);
	}

//	public Set<Player> getPlayerSet() {
//		return playerSet;
//	}

	private JButton createNextButton() {
		nextButton = new JButton(NEXT_TEXT);
		nextButton.setPreferredSize(new Dimension(110, 40));
		return nextButton;
	}
	
	public JButton getNextButton() {
		return nextButton;
	}
	
	public JButton getOpenStatsFrameBtn() {
		return nextButton;
	}	

	public BackToStartButton getBackToStartButton() {
		return backToStartButton;
	}

}
