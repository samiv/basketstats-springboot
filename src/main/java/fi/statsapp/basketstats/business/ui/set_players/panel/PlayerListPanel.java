package fi.statsapp.basketstats.business.ui.set_players.panel;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.player.PlayerListEntryCellRenderer;
import fi.statsapp.basketstats.business.ui.set_players.PlayerListButtonPanel;
import net.miginfocom.swing.MigLayout;

@Component
public class PlayerListPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	// TODO: Sisäkkäiset paneelit kind a bad thing
	private PlayerListButtonPanel playerListButtonPanel;
	private JList<Player> playerJlist;
	private DefaultListModel<Player> playerListModel;

	@Autowired
	public PlayerListPanel(PlayerListButtonPanel playerListButtonPanel) {
		this.playerListButtonPanel = playerListButtonPanel;
		initComponents();
		createPanel();
		addDoubleClickListener();
	}

	private void initComponents() {
		this.playerListModel = new DefaultListModel<Player>();
		this.playerJlist = new JList<Player>();
		this.playerJlist.setModel(this.playerListModel);
	}	

	private void createPanel() {
		setLayout(new MigLayout("", "[]", "[grow][]"));

		playerJlist.setModel(playerListModel);
		playerJlist.setVisibleRowCount(3);
		playerJlist.setCellRenderer(new PlayerListEntryCellRenderer());
		playerJlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		playerJlist.setBorder(new LineBorder(Color.BLACK));

		add(new JScrollPane(playerJlist), "grow, pushx");
		add(playerListButtonPanel, "growy");		
	}

	public DefaultListModel<Player> getPlayerListModel() {
		return playerListModel;
	}

	public List<Player> getSelectedPlayerArray() {
		List<Player> list = new ArrayList<>();
		for(int i=0; i < playerListModel.getSize(); i++){
			if(playerListModel.getElementAt(i).getSelected()){
				list.add(playerListModel.getElementAt(i));  
			}
		}
		return list;
	}

	private void addDoubleClickListener() {
		playerJlist.addMouseListener( new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					playerJlist.getSelectedValue().setSelected(!(playerJlist.getSelectedValue().getSelected()));
					playerJlist.revalidate();
					playerJlist.repaint();
				}
			}
		});
	}

	public JList<Player> getPlayerJlist() {
		return playerJlist;
	}

	public JButton getSelectButton() {
		return playerListButtonPanel.getSelectButton();
	}

	public JButton getModifyButton() {
		return playerListButtonPanel.getModifyButton();
	}

	public JButton getRemoveButton() {
		return playerListButtonPanel.getRemoveButton();
	}

	public JButton getDeSelectButton() {
		return playerListButtonPanel.getDeSelectButton();
	}
	
	public JButton getSwitchButton() {
		return playerListButtonPanel.getSwitchButton();
	}

}
