package fi.statsapp.basketstats.business.ui.results;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import fi.statsapp.basketstats.business.enums.screens.ScreenProperty;

public class StatEnumComboBox extends JComboBox<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// OISKO KUITENKIN GENEERINEN JA KÄVIS SEN LÄPI JA MUUTTAIS STRINGIKSI! EHDOTTOMASTI NÄIN
	
	public StatEnumComboBox(ScreenProperty[] values) {
		super();
		setModel(new DefaultComboBoxModel<String>(getStringArray(values)));
		insertItemAt("", values.length);
		setSelectedIndex(values.length);
	}

	private String[] getStringArray(ScreenProperty[] values) {
		String[] array = new String[values.length];
		for(int i = 0; i < values.length; ++i) {
			array[i] = values[i].getValue();
		}
		return array;
	}	
}
