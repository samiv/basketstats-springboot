package fi.statsapp.basketstats.business.ui.stats;

import java.awt.Image;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnumProvider;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;
import fi.statsapp.basketstats.business.player.Player;
import net.miginfocom.swing.MigLayout;

// TODO: Kunnon selvitys että kannattaako olla komponentti vai rakentaa joka kerta uusiksi. Sanoisin että uusiksi...
@Component
public class ModifyScreenEventFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ModifyScreenEventFrame.class);
	
	private static final String REMOVE_ICON_PATH = "icons\\remove.png";
	private static final String APPLY_ICON_PATH = "icons\\apply.png";
	private static final String CANCEL_ICON_PATH = "icons\\cancel.png";
	private static final String TITLE = "Modify Screen Properties";

	private JButton removeButton;
	private JButton applyButton;
	private JButton cancelButton;
	
	private JComboBox<ScreenPosition> posCB;
	private JComboBox<Player> userCB;
	private JComboBox<Player> setterCB;
	private JComboBox<ScreenDirection> directionCB;
	private JComboBox<ScreenDefence> defenceCB;
	private JComboBox<ScreenEnding> decisionCB;
	private JComboBox<ScreenShotClock> clockCB;
	
	private Date gameDate;
	private String ownTeamString;
	private String teamAgainstString;
	private Optional<HomeAwayEnum> homeAway;

	@Autowired
	private ScreenEnumProvider screenEnumProvider;

	public ModifyScreenEventFrame() {
		
		setLayout(new MigLayout("insets 30 20 20 20","[][grow]","[][grow]"));
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(false);
		
		createFrame();
	}
	
	public void createFrame() {
		
		JPanel fieldPanel = new JPanel(new MigLayout());	
		JPanel buttonPanel = new JPanel(new MigLayout("", "", "[][][]"));

		applyButton = new JButton("Apply");
		cancelButton = new JButton("Cancel");
		removeButton = new JButton("Remove");
		
		applyButton.setIcon(loadButtonImage(APPLY_ICON_PATH));
		cancelButton.setIcon(loadButtonImage(CANCEL_ICON_PATH));
		removeButton.setIcon(loadButtonImage(REMOVE_ICON_PATH));
		
		fieldPanel.add(createLabel("Position: "));
		posCB = new JComboBox<ScreenPosition>(ScreenPosition.values());
		fieldPanel.add(posCB, "wrap, grow");
		
		fieldPanel.add(createLabel("User #: "));
		userCB = new JComboBox<Player>();	
		fieldPanel.add(userCB, "wrap, grow");
		
		fieldPanel.add(createLabel("Setter #: "));
		setterCB = new JComboBox<Player>();		
		fieldPanel.add(setterCB, "wrap, grow");
		
		fieldPanel.add(createLabel("Direction: "));		
		directionCB = new JComboBox<ScreenDirection>(ScreenDirection.values());
		fieldPanel.add(directionCB, "wrap, grow");
		
		fieldPanel.add(createLabel("Defence: "));		
		defenceCB = new JComboBox<ScreenDefence>(ScreenDefence.values());
		fieldPanel.add(defenceCB, "wrap, grow");
		
		fieldPanel.add(createLabel("Decision: "));		
		decisionCB = new JComboBox<ScreenEnding>(ScreenEnding.values());
		fieldPanel.add(decisionCB, "wrap, grow");
		
		fieldPanel.add(createLabel("Shot Clock: "));		
		clockCB = new JComboBox<ScreenShotClock>(ScreenShotClock.values());
		fieldPanel.add(clockCB, "wrap, grow");		
		
		buttonPanel.add(removeButton, "grow, alignx left, gapright 5");
		buttonPanel.add(applyButton, "grow, alignx right, gapright 5, wrap");		
		buttonPanel.add(cancelButton, "grow, alignx right, gapright 5, wrap");		

		add(fieldPanel, "wrap");
		add(buttonPanel, "alignx right, pushx");
		
		pack();
		setLocationRelativeTo(null);
	}
	
	public void updateAndShowFrame(ScreenEvent screenEvent) {
		homeAway = screenEvent.getHomeAway();
		ownTeamString = screenEvent.getOwnTeamString();
		teamAgainstString = screenEvent.getTeamAgainstString();
		gameDate = screenEvent.getGameDate();
		posCB.setSelectedItem(screenEvent.getPosition());
		// TODO: Jos ei playeriä niin silti valitaan joku. BUGI
		userCB.setSelectedItem(new Player(screenEvent.getUserPlayerName(), screenEvent.getUserPlayerJersey()));
		setterCB.setSelectedItem(new Player(screenEvent.getSetterPlayerName(), screenEvent.getSetterPlayerJersey()));
		directionCB.setSelectedItem(screenEvent.getDirection());
		defenceCB.setSelectedItem(screenEvent.getDefenceStyle());
		decisionCB.setSelectedItem(screenEvent.getShotClock());
		clockCB.setSelectedItem(screenEvent.getShotClock());
		revalidate();
		repaint();
		setVisible(true);
	}

	public ScreenEvent getScreenEventFromFields() {
		
		return new ScreenEvent(
				getHomeAway(),
				ownTeamString,
				teamAgainstString,
				gameDate,
				posCB.getItemAt(posCB.getSelectedIndex()),
				userCB.getItemAt(userCB.getSelectedIndex()).getJersey(),
				userCB.getItemAt(userCB.getSelectedIndex()).getName(),
				setterCB.getItemAt(setterCB.getSelectedIndex()).getJersey(),
				setterCB.getItemAt(setterCB.getSelectedIndex()).getName(),
				directionCB.getItemAt(directionCB.getSelectedIndex()),
				defenceCB.getItemAt(defenceCB.getSelectedIndex()),
				decisionCB.getItemAt(decisionCB.getSelectedIndex()),
				clockCB.getItemAt(clockCB.getSelectedIndex())				
				);
	}
	
	private JLabel createLabel(String text) {
		JLabel label = new JLabel(text);
		label.setFont(label.getFont().deriveFont(13f));
		return label;
	}
	

	public JButton getRemoveButton() {
		return removeButton;
	}

	public JButton getApplyButton() {
		return applyButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}
	
	public void createPlayerComboBoxes() {
		List<Player> playerList = screenEnumProvider.getPlayerList();
		
		if (playerList.isEmpty()) {
			LOG.warn("No Players selected for keeping statistics on. Was it intentional or bug/accident");
		}
		for(Player player : playerList) {
			userCB.addItem(player);
			setterCB.addItem(player);
		}
	}

	public void setGameInfo(String ownTeamName, String teamAgainstName, Date gameDate, Optional<HomeAwayEnum> homeAway) {
		this.ownTeamString = ownTeamName;
		this.teamAgainstString= teamAgainstName;
		this.gameDate = gameDate;
		this.homeAway = homeAway;
	}

	private ImageIcon loadButtonImage(String path) {
		URL url = Thread.currentThread().getContextClassLoader().getResource(path);
		return new ImageIcon (new ImageIcon(url).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
	}
	
	private HomeAwayEnum getHomeAway() {
		if(homeAway.isPresent()) {
			return homeAway.get();
		}
		else {
			return null;
		}
	}
}
