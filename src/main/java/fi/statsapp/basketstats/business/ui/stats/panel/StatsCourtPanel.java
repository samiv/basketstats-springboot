package fi.statsapp.basketstats.business.ui.stats.panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.buttons.ScreenButton;
import net.miginfocom.swing.MigLayout;

@Component
public class StatsCourtPanel extends JPanel {

	
	private static final long serialVersionUID = 1L;
	
	private boolean isRight;
	private Image courtImageLeft;
	private Image courtImageRight;
	private JPanel contentPane;
	private JPanel flipPanel;
	private JPanel buttonPanel;
	
	private JButton flipCourtButton;

	private static ScreenButton middleButton;
	private static ScreenButton elbowLeftButton;
	private static ScreenButton elbowRightButton;
	private static ScreenButton sideLeftButton;
	private static ScreenButton sideRightButton;
	private ButtonGroup screenButtonsGroup;

	private static final String SWAP_RIGHT = ">>";
	private static final String SWAP_LEFT = "<<";
	private static final String FLIP_COURT_TEXT = "Flip court";
	private static final String COURT_LEFT_IMAGE_PATH = "icons\\courtLeft.png";
	private static final String COURT_RIGHT_IMAGE_PATH = "icons\\courtRight.png";

	@Autowired
	public StatsCourtPanel() {
		isRight = true;
		contentPane = new JPanel();
		setBorder(BorderFactory.createRaisedBevelBorder());
		getImages(COURT_LEFT_IMAGE_PATH, COURT_RIGHT_IMAGE_PATH);
		initPositionButtons();
		initFlipButton();
		createPanel();
	}

	private void getImages(String courtImageLeftPath, String courtImageRightPath) {
		ImageIcon temp = new ImageIcon(getClass().getClassLoader().getResource(courtImageLeftPath));
		this.courtImageLeft = temp.getImage();
		temp = new ImageIcon(getClass().getClassLoader().getResource(courtImageRightPath));
		this.courtImageRight = temp.getImage();		
	}

	private void initFlipButton() {
		flipCourtButton = new JButton(SWAP_LEFT);
		flipCourtButton.setPreferredSize(new Dimension(100, 40));
	}

	public JButton getFlipCourtButton() {
		return flipCourtButton;
	}

	public void flipCourtAction() {
		if(isRight) {
			flipCourtButton.setText(SWAP_RIGHT);
			setIsRight(false);
			createPanel();
		}
		else {				
			flipCourtButton.setText(SWAP_LEFT);
			setIsRight(true);
			createPanel();
		}			
	}

	// Nyt pitää huomioida että tälle paneelille on alunperin suunniteltu kaksi paneelia, toisessa screenbuttonit ja toisessa flip court.
	// Se menee tällä hetkellä sekaisi. Erikseen on sitten vielä kentän piirto joka toteutetaan rePaintissa. Päädyin nyt että yritän 
	// saada buttonit updatePanelilla, josta aina ensin poistetaan kaikki ja sitten lisätään uudet näppäimet uusissa positioissa ja tietenkin repaint
	private void createPanel() {
		contentPane.removeAll();
		contentPane.setLayout(new MigLayout("insets 5 5 20 5","[grow]","[grow][]"));
		
		buttonPanel = new JPanel(new MigLayout("", "[]", "[]20px[]45px[]45px[]20px[]"));
		
		if(isRight) {			
			buttonPanel.add(sideLeftButton, "gapleft 180, wrap");
			buttonPanel.add(elbowLeftButton, "gapleft 80, wrap");
			buttonPanel.add(middleButton, "gapleft 30, wrap");
			buttonPanel.add(elbowRightButton, "gapleft 80, wrap");
			buttonPanel.add(sideRightButton, "gapleft 180, wrap");	
		} else {
			//			setOpaque(false);
			buttonPanel.add(sideRightButton, "gapleft 0, wrap");
			buttonPanel.add(elbowRightButton, "gapleft 80, wrap");
			buttonPanel.add(middleButton, "gapleft 140, wrap");
			buttonPanel.add(elbowLeftButton, "gapleft 80, wrap");
			buttonPanel.add(sideLeftButton, "gapleft 0, wrap");
		}

		buttonPanel.setOpaque(false);
		contentPane.add(buttonPanel, "aligny top, cell 0 0");

		flipPanel = new JPanel(new MigLayout());
		flipPanel.add(flipCourtButton, "wrap");
		flipPanel.add(new JLabel(FLIP_COURT_TEXT), "alignx center");
		flipPanel.setOpaque(false);
		contentPane.add(flipPanel, "alignx center, aligny bottom, cell 0 1");
		contentPane.setOpaque(false);
		contentPane.revalidate();
		contentPane.repaint();
		
		add(contentPane);
	}

	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		super.paintComponent(g2D);
		g2D.drawImage(getCourtImage(), 0, 0, null);
		repaint();
		revalidate();
	}

	private Image getCourtImage() {
		if(this.isRight) {
			return this.courtImageRight;
		} else {
			return this.courtImageLeft;
		}
	}

	private void initPositionButtons() {
		middleButton = new ScreenButton("(3) Middle");
		elbowLeftButton = new ScreenButton("(2) Elbow L");
		elbowRightButton = new ScreenButton("(4) Elbow R");
		sideLeftButton = new ScreenButton("(1) Side L");
		sideRightButton = new ScreenButton("(5) Side R");

		// Tarvitaankohan buttonGroupia??
		screenButtonsGroup = new ButtonGroup();
		screenButtonsGroup.add(middleButton);
		screenButtonsGroup.add(elbowLeftButton);
		screenButtonsGroup.add(elbowRightButton);
		screenButtonsGroup.add(sideLeftButton);
		screenButtonsGroup.add(sideRightButton);

	}

	public void setScreenButtonsEnabled(boolean enabled) {
		middleButton.setEnabled(enabled);
		elbowLeftButton.setEnabled(enabled);
		elbowRightButton.setEnabled(enabled);
		sideLeftButton.setEnabled(enabled);
		sideRightButton.setEnabled(enabled);
		flipCourtButton.setEnabled(enabled);
	}

	boolean getIsLeft() {
		return isRight;
	}

	void setIsRight(boolean isRight) {
		this.isRight = isRight;
	}


	public ScreenButton getMiddleButton() {
		return middleButton;
	}

	public ScreenButton getElbowLeftButton() {
		return elbowLeftButton;
	}

	public ScreenButton getElbowRightButton() {
		return elbowRightButton;
	}

	public ScreenButton getSideLeftButton() {
		return sideLeftButton;
	}

	public ScreenButton getSideRightButton() {
		return sideRightButton;
	}
}
