package fi.statsapp.basketstats.business.ui.stats.panel;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class EnumPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private JTable table;
	private DefaultTableModel optionsTableModel;
	private String[] columns = new String[] {
			"Key", "Value"
	};
	
	public EnumPanel(char[] keys, String[] texts) {
		addTable(keys, texts);
	}

	private void addTable(char[] keys, String[] texts) {
		optionsTableModel = new DefaultTableModel(0, 0);
		optionsTableModel.setColumnIdentifiers(columns);
		table = new JTable(optionsTableModel);
		if(keys.length != texts.length) {
			System.out.println("keys and texts -array size not matching in EnumPanel");
		}
		int i = 0;
		for(char key : keys) {
			optionsTableModel.addRow(new Object[] {key, texts[i]});
			++i;
		}
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setEnabled(false);
		table.setFillsViewportHeight(true);
		add(table);
		
		DefaultTableCellRenderer cr = new DefaultTableCellRenderer();
		cr.setHorizontalAlignment(JLabel.CENTER);
		
		TableColumn keyCol = table.getColumnModel().getColumn(0);
		TableColumn valueCol = table.getColumnModel().getColumn(1);
		
		keyCol.setPreferredWidth(35);
		keyCol.setCellRenderer(cr);
		valueCol.setPreferredWidth(150);
	}

}
