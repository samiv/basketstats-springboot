package fi.statsapp.basketstats.business.ui.stats.panel;

import java.awt.Color;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnumProvider;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;
import fi.statsapp.basketstats.business.service.ScreenEventService;
import fi.statsapp.basketstats.business.ui.stats.AbstractStatsTextField;
import fi.statsapp.basketstats.business.ui.stats.PositionTextField;
import fi.statsapp.basketstats.business.ui.stats.StatsInfoLabel;
import fi.statsapp.basketstats.business.ui.stats.StatsTextField;
import net.miginfocom.swing.MigLayout;

@Component
public class EnterStatsPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(EnterStatsPanel.class);

	// TODO: Tässäkin paneelit vähän hassusti, voisiko frameen sijoittaa layoutin ja kaikille näille paneeleille omat beanit?
	private ActivityPanel activityPanel;
	private StatsCourtPanel statsCourtPanel;
	private JPanel optionsPanel;
	private JPanel setStatsPanel;
	private ScreenEnumProvider screenEnumProvider;
	private ScreenEventService screenEventService;

	private static final String SCREEN_POSITION_LBL = "Screen position: ";
	private static final String SET_USER_LBL = "Set user (#): ";
	private static final String SET_SETTER_LBL = "Set setter (#): ";
	private static final String SET_DIRECTION_LBL = "Set direction: ";
	private static final String SET_DEFENCE_LBL = "Set defence: ";
	private static final String SET_DECISION_LBL = "Set decision: ";
	private static final String SET_SHOTCLOCK_LBL = "Set shot clock: ";	
	private static final String DISCARD_BUTTON_TEXT = "(F1) Discard";

	private Optional<HomeAwayEnum> homeAway;
	private PositionTextField positionField;
	private StatsTextField setSetterField;
	private StatsTextField setDirectionField;
	private StatsTextField setUserField;
	private StatsTextField setDefenceField;
	private StatsTextField setDecisionField;
	private StatsTextField setClockField;

	private boolean addingElement;
	private JLabel screenPositionLabelText;
	private StatsInfoLabel statsInfoLabel;

	private JButton discardButton;

	private String ownTeamString;
	private String teamAgainstString;
	private Date gameDate;

	@Autowired
	public EnterStatsPanel(StatsCourtPanel statsCourtPanel, ScreenEnumProvider screenEnumProvider,
			ScreenEventService screenEventService, ActivityPanel activityPanel) {
		this.statsCourtPanel = statsCourtPanel;
		this.screenEnumProvider = screenEnumProvider;
		this.screenEventService = screenEventService;
		this.activityPanel = activityPanel;
		setLayout(new MigLayout("insets 30 20 0 20","[400px][]","[]10px[]"));
		setBorder(BorderFactory.createLineBorder(Color.black));
		createPanel();
	}

	private void createPanel() {

		setStatsPanel = new JPanel(new MigLayout("", "", ""));

		// Paneeli laitetaan scrolliin joka laitetaan paneeliin?
		// voisko optionsPanel olla scrolli ja meneekö se nätisti EnterStatsPaneeliin. Toinen paneeli vois jotenkin kyllä poistua
		optionsPanel = new JPanel(new MigLayout());

		// Ei toimi boldaus :( jotenkin pitäisi saada toimimaan. Mutta se siis tuolla update-metodissa mykyään
		// MatchInfoPanelissa toteutettu joku fonttikikkailu
		screenPositionLabelText = new JLabel(SCREEN_POSITION_LBL);
		positionField = new PositionTextField(this);
		setStatsPanel.add(positionField.getCheckBox());
		setStatsPanel.add(screenPositionLabelText);
		setStatsPanel.add(positionField, "wrap");

		// Text field initialization
		initTextFields();

		// Add optionsPanel to EnterStatsPanel
		add(optionsPanel, "cell 1 0");

		// Discard button
		discardButton = new JButton(DISCARD_BUTTON_TEXT);
		setStatsPanel.add(discardButton, "wrap, cell 1 100");
		statsInfoLabel = new StatsInfoLabel();
		add(statsInfoLabel, "aligny top, pushy, cell 0 1");

		// Add setStatsPanel to EnterStatsPanel
		add(setStatsPanel, "cell 0 0, alignx center, aligny top, growx");

		showDefaultOptionsPanel();

	}

	private void initTextFields() {

		setUserField = new StatsTextField(optionsPanel, screenEnumProvider.getPlayerListScroll(), this);
		setSetterField = new StatsTextField(optionsPanel, screenEnumProvider.getPlayerListScroll(), this);
		setDirectionField = new StatsTextField(optionsPanel, screenEnumProvider.getDirectionOptionsScroll(), this);
		setDefenceField = new StatsTextField(optionsPanel,screenEnumProvider.getDefenceOptionsScroll(), this);
		setDecisionField = new StatsTextField(optionsPanel, screenEnumProvider.getDesicionOptionsScroll(), this);
		setClockField = new StatsTextField(optionsPanel, screenEnumProvider.getShotClockOptionsScroll(), this);

		positionField.setNextField(setUserField);
		setUserField.setPreviousAndNextField(null, setSetterField);
		setSetterField.setPreviousAndNextField(setUserField, setDirectionField);
		setDirectionField.setPreviousAndNextField(setSetterField, setDefenceField);
		setDefenceField.setPreviousAndNextField(setDirectionField, setDecisionField);
		setDecisionField.setPreviousAndNextField(setDefenceField, setClockField);
		setClockField.setPreviousAndNextField(setDecisionField, null);

		setStatsPanel.add(setUserField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_USER_LBL));
		setStatsPanel.add(setUserField, "wrap");

		setStatsPanel.add(setSetterField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_SETTER_LBL));
		setStatsPanel.add(setSetterField, "wrap");

		setStatsPanel.add(setDirectionField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_DIRECTION_LBL));
		setStatsPanel.add(setDirectionField, "wrap");

		setStatsPanel.add(setDefenceField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_DEFENCE_LBL));
		setStatsPanel.add(setDefenceField, "wrap");

		setStatsPanel.add(setDecisionField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_DECISION_LBL));
		setStatsPanel.add(setDecisionField, "wrap");

		setStatsPanel.add(setClockField.getCheckBox());
		setStatsPanel.add(new JLabel(SET_SHOTCLOCK_LBL));
		setStatsPanel.add(setClockField, "wrap");

	}

	public StatsTextField getSetUserField() {
		return setUserField;
	}
	
	public StatsTextField getSetSetterField() {
		return setSetterField;
	}

	public void showDefaultOptionsPanel() {
		optionsPanel.removeAll();
		optionsPanel.add(screenEnumProvider.getPlayerListScroll());
	}
	
	public JScrollPane getPlayerListScroll() {
		return screenEnumProvider.getPlayerListScroll();
	}

	public void discardEvent() {
		addingElement = false;
		setFieldsEnabled(false);
		setCheckBoxesEnabled(true);
		emptyTextFields();
		this.statsCourtPanel.setScreenButtonsEnabled(true);
		showDefaultOptionsPanel();
		if(!positionField.getCheckBox().isSelected()) {
			AbstractStatsTextField firstEnabled = positionField.findNextSelected();
			if (firstEnabled != null) {
				firstEnabled.setEnabled(true);
				firstEnabled.requestFocus();
			}
			else {
				LOG.warn("PositionField is not selected and firstEnabled field not found. This means that none of the stats is selected!?");
			}
		}
	}

	public StatsCourtPanel getStatsCourtPanel() {
		return statsCourtPanel;
	}

	public void setFieldsEnabled(boolean enabled) {
		positionField.setEnabled(enabled);
		setSetterField.setEnabled(enabled);
		setDirectionField.setEnabled(enabled);
		setUserField.setEnabled(enabled);
		setDefenceField.setEnabled(enabled);
		setDecisionField.setEnabled(enabled);
		setClockField.setEnabled(enabled);
	}

	public void setCheckBoxesEnabled(boolean enabled) {
		positionField.getCheckBox().setEnabled(enabled);
		setUserField.getCheckBox().setEnabled(enabled);
		setSetterField.getCheckBox().setEnabled(enabled);
		setDirectionField.getCheckBox().setEnabled(enabled);
		setDefenceField.getCheckBox().setEnabled(enabled);
		setDecisionField.getCheckBox().setEnabled(enabled);
		setClockField.getCheckBox().setEnabled(enabled);
	}

	public void emptyTextFields() {
		positionField.setText("");
		setUserField.setText("");
		setSetterField.setText("");
		setDirectionField.setText("");
		setDefenceField.setText("");
		setDecisionField.setText("");
		setClockField.setText("");		
	}

	public JButton getDiscardButton() {
		return discardButton;
	}
	public boolean isAddingElement() {
		return addingElement;
	}

	public StatsInfoLabel getStatsInfoLabel() {
		return statsInfoLabel;
	}

	public void setAddingElement(boolean addingElement) {
		this.addingElement = addingElement;
	}

	public void updateScreenPositionLabelString(String screenPositionLabelString) {
		this.positionField.setLabelText(screenPositionLabelString);
	}
	
	public AbstractStatsTextField getFirstField() {
		return positionField;
	}

	public void saveEntity() {
		ScreenEvent screenEventToSave = getEventFromFields();
		screenEventService.save(screenEventToSave);
		List<ScreenEvent> lista = screenEventService.findAll();
		for (ScreenEvent event : lista) {
			System.out.println(event.toString());
		}
		// Add to recent list
		activityPanel.getActivityList().clearSelection();
		activityPanel.addScreenEvent(screenEventToSave);
		statsInfoLabel.setSuccess("Onnistuttiin!!!!!!!");
		revalidate();
		repaint();
	}

	private ScreenEvent getEventFromFields() {
		return new ScreenEvent(
				homeAway.orElse(null),
				Optional.ofNullable(ownTeamString).orElse(null),
				Optional.ofNullable(teamAgainstString).orElse(null),
				Optional.ofNullable(gameDate).orElse(null),
				ScreenPosition.returnPosFromString(getPositionFromField()),
				getJerseyFromField(setSetterField),
				screenEnumProvider.getPlayerName(getJerseyFromField(setSetterField)),
				getJerseyFromField(setUserField),
				screenEnumProvider.getPlayerName(getJerseyFromField(setUserField)),
				ScreenDirection.returnDirFromChar(getCharFromField(setDirectionField)),
				ScreenDefence.returnDefFromChar(getCharFromField(setDefenceField)),
				ScreenEnding.returnDecFromChar(getCharFromField(setDecisionField)),
				ScreenShotClock.returnTimeFromChar(getCharFromField(setClockField))
				);
	}
	
	private Integer getJerseyFromField(StatsTextField field) {
		String str = field.getKeyChar();
		if(str == null || str.isEmpty()) {
			return null;
		}
		Integer setJer = Integer.parseInt(str);
		return setJer == null ? null : setJer;
	}
	
	private String getPositionFromField() {
		String posString= positionField.getLabelText();
		if (posString == null || posString.isEmpty()) {
			return null;
		}
		return posString;
	}
	
	private Character getCharFromField(StatsTextField field) {
		String str = field.getKeyChar();
		if (str == null || str.isEmpty()) {
			return null;
		}
		return str.toUpperCase().charAt(0);
	}

	public void setGameInfo(String ownTeamName, String teamAgainstName, Date gameDate, Optional<HomeAwayEnum> homeAway) {
		this.ownTeamString = ownTeamName;
		this.teamAgainstString = teamAgainstName;
		this.gameDate = gameDate;
		this.homeAway = homeAway;
	}

	// TODO: Ois tää hyvä saada joskus valmiiks
	public void updatePlayerScrolls() {
		setUserField.setScrollPanel(screenEnumProvider.getPlayerListScroll());
		setSetterField.setScrollPanel(screenEnumProvider.getPlayerListScroll());
	}

	public void setFirstCheckBoxEnabledAndFocusAndOptionsPanel() {
		AbstractStatsTextField field = setUserField;
		while(true) {
			if(field.getCheckBox().isSelected()) {
				break;
			}
			else {
				field = field.getNextField();
			}
		}
		field.setEnabled(true);
		field.requestFocus();
		
		if(field.getPreviousField() == null || field.getPreviousField().getPreviousField() == null) {
			showDefaultOptionsPanel();
		}
		else {
			optionsPanel.removeAll();
			optionsPanel.add(field.getScrollPanel());
			optionsPanel.revalidate();
			optionsPanel.repaint();
		}
	}
	
	// TODO: Nämä johonkin utilssiin koska käyttää muuallakin
	public String getIfNotNull(String value) {
		return value == null ? null : value;
	}

	public HomeAwayEnum getIfNotNull(Optional<HomeAwayEnum> homeAway) {
		return homeAway.isPresent() ? homeAway.get() : null;
	}
	
	public Date getIfNotNull(Date date) {
		return date == null ? null : date;
	}
}
