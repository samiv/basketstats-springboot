package fi.statsapp.basketstats.business.ui.results;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.buttons.BackToStartButton;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;
import fi.statsapp.basketstats.business.player.HomeAwayRadioButtonPanel;
import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.player.Team;
import fi.statsapp.basketstats.business.player.TeamStore;
import net.miginfocom.swing.MigLayout;

@Component
public class ResultsPanel extends JPanel {	

	private static final long serialVersionUID = 1L;

	private static final String SELECT_OWN_TEAM_LBL = "Select Own Team:";
	private static final String UPDATE_BUTTON_LBL = "Update";
	private static final String SELECT_TEAM_AGAINST_LBL = "Select Team Against:";
	private static final String SCREEN_POSITION_LBL = "Screen position: ";
	private static final String USER_LBL = "User (#): ";
	private static final String SETTER_LBL = "Setter (#): ";
	private static final String DIRECTION_LBL = "Direction: ";
	private static final String DEFENCE_LBL = "Defence: ";
	private static final String DECISION_LBL = "Decision: ";
	private static final String SHOTCLOCK_LBL = "Shot clock: ";
	private static final String GET_RESULTS_TEXT = "Get Results";

	private HomeAwayRadioButtonPanel homeAwayPanel;
	private JComboBox<Team> selectOwnTeamCB;
	private JButton updateTeam;
	private JComboBox<Team> selectTeamAgainstCB;
	private StatEnumComboBox positionCB;
	private JComboBox<Player> userCB;
	private JComboBox<Player> setterCB;
	private StatEnumComboBox directionCB;
	private StatEnumComboBox defenceCB;
	private StatEnumComboBox endingCB;
	private StatEnumComboBox shotClockCB;

	private BackToStartButton backButton;
	private JButton getResultsButton;

	public ResultsPanel() {
		setUpPanel();
	}

	private void setUpPanel() {
		setLayout(new MigLayout("", "[]", "[]20[]20[]40[]"));

		this.backButton = new BackToStartButton();
		updateTeam = new JButton(UPDATE_BUTTON_LBL);
		this.getResultsButton = new JButton(GET_RESULTS_TEXT);
		//		setLayout(new MigLayout("insets 10", "[][]", "[][][][][][][][]"));
	}

	private void initComponents(TeamStore teamStore) {		

		removeAll();
		JPanel searchFieldsPanel = new JPanel(new MigLayout());
		
		createTeamComboBoxes(teamStore);
		
		// TODO: Kyllä noi StatEnumComboBoxit jotenkin saa genericsien kautta, sais samatta getSelectedItem() toteutettua sinne.
		homeAwayPanel = new HomeAwayRadioButtonPanel();
		positionCB = new StatEnumComboBox(ScreenPosition.values());
		userCB = new JComboBox<Player>();
		setterCB = new JComboBox<Player>();
		directionCB = new StatEnumComboBox(ScreenDirection.values());
		defenceCB = new StatEnumComboBox(ScreenDefence.values());
		endingCB = new StatEnumComboBox(ScreenEnding.values());
		shotClockCB = new StatEnumComboBox(ScreenShotClock.values());

		searchFieldsPanel.add(homeAwayPanel, "wrap, cell 1 0");
		searchFieldsPanel.add(new JLabel(SELECT_OWN_TEAM_LBL));
		searchFieldsPanel.add(selectOwnTeamCB, "growx");
		searchFieldsPanel.add(updateTeam, "wrap");
		searchFieldsPanel.add(new JLabel(SELECT_TEAM_AGAINST_LBL));
		searchFieldsPanel.add(selectTeamAgainstCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(SCREEN_POSITION_LBL));
		searchFieldsPanel.add(positionCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(USER_LBL));
		searchFieldsPanel.add(userCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(SETTER_LBL));
		searchFieldsPanel.add(setterCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(DIRECTION_LBL));
		searchFieldsPanel.add(directionCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(DEFENCE_LBL));
		searchFieldsPanel.add(defenceCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(DECISION_LBL));
		searchFieldsPanel.add(endingCB, "wrap, growx");
		searchFieldsPanel.add(new JLabel(SHOTCLOCK_LBL));
		searchFieldsPanel.add(shotClockCB, "wrap, growx");
		add(searchFieldsPanel, "cell 0 1");

		add(getResultsButton, "cell 0 2, alignx left, aligny top, pushy");
		add(backButton, "cell 0 3, alignx left, aligny bottom, pushy");
		
//		revalidate();
//		repaint();
	}

	private void createTeamComboBoxes(TeamStore teamStore) {
		selectOwnTeamCB = new JComboBox<Team>();		
		selectTeamAgainstCB = new JComboBox<Team>();
		for(Team team : teamStore.getTeamList()) { 
			selectOwnTeamCB.addItem(team); 
			selectTeamAgainstCB.addItem(team); 
		}
		
		selectOwnTeamCB.insertItemAt(new Team(null, null), 0);
		selectOwnTeamCB.setSelectedIndex(0);
		selectTeamAgainstCB.insertItemAt(new Team(null, null), 0);
		selectTeamAgainstCB.setSelectedIndex(0);
	}

	public void addTeamsAndPlayerToComboBoxes(TeamStore teamStore) {
		initComponents(teamStore);
	}

	public JComboBox<Team> getSelectOwnTeamCB() {
		return selectOwnTeamCB;
	}

	public JComboBox<Team> getSelectTeamAgainstCB() {
		return selectTeamAgainstCB;
	}

	public BackToStartButton getBackToStartButton() {
		return this.backButton;
	}

	public JButton getGetResultsButton() {
		return getResultsButton;
	}

	public StatEnumComboBox getPositionCB() {
		return positionCB;
	}

	public JComboBox<Player> getUserCB() {
		return userCB;
	}

	public JComboBox<Player> getSetterCB() {
		return setterCB;
	}

	public StatEnumComboBox getDirectionCB() {
		return directionCB;
	}

	public StatEnumComboBox getDefenceCB() {
		return defenceCB;
	}

	public StatEnumComboBox getEndingCB() {
		return endingCB;
	}

	public StatEnumComboBox getShotClockCB() {
		return shotClockCB;
	}

	public JButton getUpdateTeam() {
		return updateTeam;
	}
	
	public HomeAwayRadioButtonPanel getHomeAwayPanel() {
		return homeAwayPanel;
	}

}
