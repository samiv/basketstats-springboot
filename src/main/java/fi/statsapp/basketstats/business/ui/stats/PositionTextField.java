package fi.statsapp.basketstats.business.ui.stats;

import java.awt.Color;

import javax.swing.JCheckBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.statsapp.basketstats.business.ui.stats.panel.EnterStatsPanel;

public class PositionTextField extends AbstractStatsTextField {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(PositionTextField.class);

	private EnterStatsPanel enterStatsPanel;

	public PositionTextField(EnterStatsPanel enterStatsPanel) {
		super();
		this.enterStatsPanel = enterStatsPanel;
	}

	@Override
	public void initTextFieldField() {
		setColumns(5);
		setEnabled(false);
	}

	@Override
	public void initCheckBox() {
		this.checkBox = new JCheckBox("");
		this.checkBox.setSelected(true);
		this.checkBox.addActionListener(e -> {
			AbstractStatsTextField next = findNextSelected();
			if(!this.checkBox.isSelected()) {
				setText("");
				setBackground(Color.GRAY);
				enterStatsPanel.getStatsCourtPanel().setScreenButtonsEnabled(false);
				enterStatsPanel.setAddingElement(true);
				if(next != null) {
					next.setEnabled(true);
					next.requestFocus();
				}
				else {
					LOG.warn("Trying to find next Selected from Positionfield");
					enterStatsPanel.setFieldsEnabled(false);
				}

			}
			else {
				setBackground(Color.WHITE);
				enterStatsPanel.getStatsCourtPanel().setScreenButtonsEnabled(true);
				enterStatsPanel.setAddingElement(true);
				enterStatsPanel.setFieldsEnabled(false);
				requestFocus();
			}
		});
	}

}
