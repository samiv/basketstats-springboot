package fi.statsapp.basketstats.business.ui.stats;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.statsapp.basketstats.business.ui.stats.panel.EnterStatsPanel;

public class StatsTextField extends AbstractStatsTextField {	

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(StatsTextField.class);

	private EnterStatsPanel enterStatsPanel;

	// Ei taideta käyttää, tekstit syötetään suoraan enterStatsPaneelissa

	private JPanel optionsPanel;
	private String keyChar;

	public StatsTextField(JPanel optionsPanel, JScrollPane scroll, EnterStatsPanel enterStatsPanel) {
		super();
		this.enterStatsPanel = enterStatsPanel;	
		this.optionsPanel = optionsPanel;
		this.optionsScrollPanel = scroll;
	}

	public StatsTextField(JPanel optionsPanel, EnterStatsPanel enterStatsPanel) {
		super();
		this.enterStatsPanel = enterStatsPanel;	
		this.optionsPanel = optionsPanel;
	}

	public void initTextFieldField() {
		setColumns(3);
		setEnabled(false);
		addActionListener(e -> {

			// TODO: keychar ei kuvaa kovin hyvin koska laitetaan myös numeroita. Pitäisikö muutenkin tarkistaa että muissa on vain yksi kirjain????
			keyChar = getText();
			AbstractStatsTextField nextEnabled;

			// TODO: Tätä logiikkaa voisi vähän kommentoida ettei tartte miettiä joka kerta uusiksi

			nextEnabled = nextField;
			while(true) {
				// Tämä on nyt case viimeinen kenttä. Ja myös sitten se tilanne että on viimeinen enabloitu kenttä
				if(nextEnabled == null) {
					setEnabled(false);
					// TODO: Periaatteessa nää kaikki controlleriin...
					enterStatsPanel.setAddingElement(false);
					enterStatsPanel.emptyTextFields();
					enterStatsPanel.getStatsCourtPanel().setScreenButtonsEnabled(true);
					enterStatsPanel.setCheckBoxesEnabled(true);
					enterStatsPanel.setFieldsEnabled(false);
					enterStatsPanel.showDefaultOptionsPanel();
					enterStatsPanel.saveEntity();
					return;
				}
				if (!nextEnabled.getCheckBox().isSelected()) {
					nextEnabled = nextEnabled.getNextField();
				}
				else {
					break;
				}
			}

			if (previousField == null) {
				setEnabled(false);
				nextEnabled.setEnabled(true);
				nextEnabled.requestFocus();
				// TODO: Nämä pitäisi saada toimimaan sillei että MainMenuControllerissa asetetut ScrollPanelit toimisi
				//				optionsPanel.removeAll();
				//				optionsPanel.add(nextEnabled.getScrollPanel());
				if(nextField.getCheckBox().isSelected()) {
					enterStatsPanel.showDefaultOptionsPanel();
				}
				else {
					optionsPanel.removeAll();
					optionsPanel.add(nextEnabled.getScrollPanel());
				}

			} else {
				setEnabled(false);
				nextEnabled.setEnabled(true);
				nextEnabled.requestFocus();
				optionsPanel.removeAll();
				optionsPanel.add(nextEnabled.getScrollPanel());
			}

			optionsPanel.revalidate();
			optionsPanel.repaint();
		});

		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {

				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					AbstractStatsTextField previousEnabled = findPreviousSelected();
					if(previousEnabled == null) {
						enterStatsPanel.discardEvent();
						enterStatsPanel.showDefaultOptionsPanel();
						// TODO: SEMI-PURKKA. Toimii, mutta todellista syytä en tiedä, että miksi playerScroll häviää escittäessä.
						// TODO: TÄHÄN EHOTTOMASTI JOKU FUNKTIO JOKA HAKEE OIKEAN SCROLLPANELIN JA PALAUTTAA DEFAULTIN TIETYISSÄ TILANTEISSA
						// TODO: SIELLÄ FUNKTIOSSA KAIKKI NÄMÄ PREIVIOUS.PREVIOUS
					} 
					//					else if(previousEnabled.getPreviousField() == null || previousEnabled.getPreviousField().getPreviousField() == null) {
					//						optionsPanel.removeAll();
					//						setEnabled(false);
					//						setText("");
					//						previousEnabled.setText("");
					//						previousEnabled.setEnabled(true);
					//						previousEnabled.requestFocus();
					//						enterStatsPanel.showDefaultOptionsPanel();
					//					} 
					else {
						optionsPanel.removeAll();
						setEnabled(false);
						setText("");
						previousEnabled.setText("");
						previousEnabled.setEnabled(true);
						previousEnabled.requestFocus();
						optionsPanel.add(previousEnabled.getScrollPanel());
					}
				}

				if (e.getKeyCode() == KeyEvent.VK_F1) {
					enterStatsPanel.discardEvent();
					enterStatsPanel.showDefaultOptionsPanel();
				}

				optionsPanel.revalidate();
				optionsPanel.repaint();
			}

			@Override
			public void keyPressed(KeyEvent e) {				
			}
		} );
	}

	public String getKeyChar() {
		return keyChar;
	}

	@Override
	public JScrollPane getScrollPanel() {
		if(getPreviousField() == null || getPreviousField().getPreviousField() == null) {
			return enterStatsPanel.getPlayerListScroll();
		}
		else {
			return this.optionsScrollPanel;
		}
	}

	@Override
	public void initCheckBox() {
		this.checkBox = new JCheckBox("");
		this.checkBox.setSelected(true);
		this.checkBox.addActionListener(e -> {
			boolean isAdding = enterStatsPanel.isAddingElement();
			AbstractStatsTextField focusedField = enterStatsPanel.getFirstField().findNextSelected();
			if (focusedField != null) {
				if(!this.checkBox.isSelected()) {
					setText("");
					setBackground(Color.GRAY);
				}
				else {
					setBackground(Color.WHITE);
				}

				if(isAdding) {
					// TODO: Joku addPanel-funktio joka tekisi nää removet ja revalidatet
					optionsPanel.removeAll();
					enterStatsPanel.setFieldsEnabled(false);
					focusedField.setEnabled(true);
					optionsPanel.add(focusedField.getScrollPanel());
					focusedField.requestFocus();
					optionsPanel.revalidate();
					optionsPanel.repaint();
				}
			}
			else {
				LOG.warn("Trying to find next Selected, but this is the last field");
				enterStatsPanel.setFieldsEnabled(false);
				setBackground(Color.GRAY);
			}

		});
	}


}
