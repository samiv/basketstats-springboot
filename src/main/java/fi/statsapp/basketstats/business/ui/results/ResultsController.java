package fi.statsapp.basketstats.business.ui.results;

import java.util.List;

import javax.swing.JComboBox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;
import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.player.Team;
import fi.statsapp.basketstats.business.player.TeamStore;
import fi.statsapp.basketstats.business.service.ScreenEventService;
import fi.statsapp.basketstats.business.ui.shared.controller.AbstractFrameController;

@Controller
public class ResultsController extends AbstractFrameController {

	private ResultsFrame resultsFrame;
	private ScreenEventService screenEventService;
	private boolean isOpened = false;
	
	@Autowired
	public ResultsController(ResultsFrame resultsFrame, ScreenEventService screenEventService) {
		this.resultsFrame = resultsFrame;
		this.screenEventService = screenEventService;
	}
	
	@Override
	public void prepareAndOpenFrame() {
		resultsFrame.setVisible(true);
		registerAction(resultsFrame.getResultsPanel().getGetResultsButton(), (e) -> getResults());
		
		registerAction(getResultsFrame().getResultsPanel().getUpdateTeam(), (e) -> updatePlayerComboBoxes());
	}
	
	public void prepareAndOpenFrame(TeamStore teamStore) {
		if (!isOpened) {
			resultsFrame.getResultsPanel().addTeamsAndPlayerToComboBoxes(teamStore);
			isOpened = true;
		}
		prepareAndOpenFrame();
	}
	
	private void updatePlayerComboBoxes() {
		Team team = (Team) getResultsFrame().getResultsPanel().getSelectOwnTeamCB().getSelectedItem();
		// Voisko näistä tehdä statEnumeita?
		JComboBox<Player> userCB = getResultsFrame().getResultsPanel().getUserCB();
		JComboBox<Player> setterCB = getResultsFrame().getResultsPanel().getSetterCB();
		
		userCB.removeAllItems();
		setterCB.removeAllItems();
		int playerListSize = 0;
		for(Player player : team.getPlayerList()) {
			userCB.addItem(player);
			setterCB.addItem(player);
			playerListSize++;
		}
		
		// TODO: pitäiskö jotenkin laittaa empty-Player. Joku vakio emptyPlayer? nullien kanssa ei kannattaisi pelleillä
		userCB.insertItemAt(new Player(null, null), playerListSize);
		userCB.setSelectedItem(userCB.getItemAt(playerListSize));
		setterCB.insertItemAt(new Player(null, null), playerListSize);
		setterCB.setSelectedItem(setterCB.getItemAt(playerListSize));
	}

	
	private void getResults() {		

		// Jotenkin tuntuu tyhmältä ottaa täällä Stringejä comboboxeista ja muuttaa ne Enumeiks
		
		ResultsPanel resultsPanel = resultsFrame.getResultsPanel();
		
		HomeAwayEnum homeAway = resultsPanel.getHomeAwayPanel().getSelectedHomeAway();
		
		String ownTeamString = ((Team) resultsPanel.getSelectOwnTeamCB().getSelectedItem()).getTeamName();
		
		String teamAgainstString = ((Team) resultsPanel.getSelectTeamAgainstCB().getSelectedItem()).getTeamName();
		
		ScreenPosition position = ScreenPosition.returnPosFromString((String) resultsPanel.getPositionCB().getSelectedItem());

		Player userPlayer = (Player) resultsPanel.getUserCB().getSelectedItem();
		Integer user_jersey;
		
		if (userPlayer != null) {
		    user_jersey = userPlayer.getJersey();			
		} else {
		    user_jersey = null; 
		}
		
		Player setterPlayer = (Player) resultsPanel.getUserCB().getSelectedItem();
		Integer setter_jersey;
	    
		if (setterPlayer != null) {
		    setter_jersey = setterPlayer.getJersey();			
		} else {
		    setter_jersey = null; 
		}
		
		ScreenDirection dir = ScreenDirection.returnDirectionFromString((String) resultsPanel.getDirectionCB().getSelectedItem());

		ScreenDefence def = ScreenDefence.returnDefenceFromString((String) resultsPanel.getDefenceCB().getSelectedItem());
		
		ScreenEnding ending = ScreenEnding.returnEndingFromString((String) resultsPanel.getEndingCB().getSelectedItem());
		
		ScreenShotClock clock = ScreenShotClock.returnShotClockFromString((String) resultsPanel.getShotClockCB().getSelectedItem());
		
		List<ScreenEvent> a = screenEventService.findStatResultsFromRepository(
				homeAway,
				ownTeamString,
				teamAgainstString,
				null,
				position,
				user_jersey,
				setter_jersey,
				dir,
				def,
				ending,
				clock);
		
		for (ScreenEvent e : a) {
			System.out.println(e);
		}
		
		resultsFrame.addResultsToList(a);
		
//		return screenEventService.findStatResultsFromRepository(position, user_jersey, setter_jersey, dir, def, ending, clock);
	}

	public ResultsFrame getResultsFrame() {
		return resultsFrame;
	}
	

}
