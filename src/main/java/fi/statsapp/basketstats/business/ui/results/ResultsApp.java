package fi.statsapp.basketstats.business.ui.results;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import fi.statsapp.basketstats.AbstractAppFrame;
import fi.statsapp.basketstats.business.buttons.BackToStartButton;

public class ResultsApp extends AbstractAppFrame {
	private static final long serialVersionUID = 1L;

	private static final String RESULTS_FRAME_TITLE_TEXT = "Results";
	
	private static JFrame resultsFrame;
	
	public ResultsApp() {
		initialize();
	}

	public void initialize() {
		resultsFrame = new JFrame();
		resultsFrame.setTitle(RESULTS_FRAME_TITLE_TEXT);
		resultsFrame.setBounds(100, 100, getAppSizeX(), getAppSizeY());
		resultsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		resultsFrame.getContentPane().setLayout(null);
		
		this.backButton = new BackToStartButton();
		resultsFrame.getContentPane().add(this.backButton);
		this.backButton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				resultsFrame.setVisible(false);
				resultsFrame.dispose();				
			}
		});
		
	}
	
	public JFrame getResultsFrame() {
		return resultsFrame;
	}

}
