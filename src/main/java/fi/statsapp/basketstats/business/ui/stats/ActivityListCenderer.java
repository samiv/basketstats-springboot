package fi.statsapp.basketstats.business.ui.stats;

import java.awt.Color;
import java.awt.Component;
import java.util.Optional;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import fi.statsapp.basketstats.business.entity.ScreenEvent;

public class ActivityListCenderer extends JLabel implements ListCellRenderer<ScreenEvent> {

	// TODO: oisko helpompi extendata JList niinkuin ResultsCellRendererissa? -> Ei ehkä toimi taustavärit

	private static final String NO_STAT_STRING_TO_RESULTS_LIST = "--";
	private static final long serialVersionUID = 1L;	
	private JLabel label;
	
	public ActivityListCenderer() {
		label = new JLabel();
		label.setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends ScreenEvent> list, ScreenEvent value, int index,
			boolean isSelected, boolean cellHasFocus) {
		
		String setJer = value.getUserPlayerJersey() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getUserPlayerJersey().toString();
		String useJer = value.getUserPlayerJersey() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getUserPlayerJersey().toString();
		String pos = value.getPosition() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getPosition().toString();
		String dir = value.getDirection() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getDirection().toString();
		String def = value.getDefenceStyle() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getDefenceStyle().toString();
		String end = value.getEnding() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getEnding().toString();
		String clk = value.getShotClock() == null ? NO_STAT_STRING_TO_RESULTS_LIST : value.getShotClock().toString();
		
		label.setText(
				"#" + setJer +
				",   # " + useJer +
				",   " + pos +
				",   " + dir +
				",   " + def +
				",   " + end +
				",   " + clk
				);
		
		if (isSelected) {
	         label.setBackground(Color.BLUE);
	         label.setForeground(Color.ORANGE);
	      }
	      else {
	    	  label.setBackground(list.getBackground());
	    	  label.setForeground(list.getForeground());
	      }
	  
	      label.setEnabled(list.isEnabled());
	      label.setFont(list.getFont());		
		
		return label;
	}

}
