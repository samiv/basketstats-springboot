package fi.statsapp.basketstats.business.ui.set_players;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import net.miginfocom.swing.MigLayout;

@Component
public class PlayerListButtonPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private JButton selectButton;
	private JButton modifyButton;
	private JButton removeButton;
	private JButton deSelectButton;
	private JButton switchButton;
	
	private static final String REMOVE_TEXT = "Remove";
	private static final String MODIFY_TEXT = "Modify";
	private static final String SELECT_TEXT = "Select";
	private static final String DESELECT_TEXT = "Deselect";
	private static final String SELECT_DESELECT_ALL = "Switch All";
	
	public PlayerListButtonPanel() {
		setLayout(new MigLayout("flowy, gapy 9px", "[]", "[grow]"));
		this.selectButton = new JButton(SELECT_TEXT, new ImageIcon(getClass().getClassLoader().getResource("icons\\selected.png")));
		this.removeButton = new JButton(REMOVE_TEXT);
		this.modifyButton = new JButton(MODIFY_TEXT);
		this.deSelectButton = new JButton(DESELECT_TEXT, new ImageIcon(getClass().getClassLoader().getResource("icons\\deselected.png")));
		this.switchButton = new JButton(SELECT_DESELECT_ALL, new ImageIcon(getClass().getClassLoader().getResource("icons\\switch.png")));
		
		selectButton.setPreferredSize(new Dimension(100, 35));
		removeButton.setPreferredSize(new Dimension(100, 35));
		modifyButton.setPreferredSize(new Dimension(100, 35));
		deSelectButton.setPreferredSize(new Dimension(100, 35));
		switchButton.setPreferredSize(new Dimension(100, 35));
		
		// TODO: tämä läyout tuntuu menevän enemmän tuurilla kuin taidolla...
		add(selectButton, "growx,aligny bottom");
		add(deSelectButton, "growx");
		add(switchButton, "growx, aligny top");
		add(modifyButton, "growx,aligny bottom");
		add(removeButton, "growx,aligny top");
	}

	public JButton getSelectButton() {
		return selectButton;
	}
	public JButton getModifyButton() {
		return modifyButton;
	}

	public JButton getRemoveButton() {
		return removeButton;
	}

	public JButton getDeSelectButton() {
		return deSelectButton;
	}
	
	public JButton getSwitchButton() {
		return switchButton;
	}

}
