package fi.statsapp.basketstats.business.ui.stats;

import java.util.Date;
import java.util.Optional;

import javax.swing.JList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.service.ScreenEventService;
import fi.statsapp.basketstats.business.ui.shared.controller.AbstractFrameController;
import fi.statsapp.basketstats.business.ui.stats.frame.StatsAppFrame;

@Controller
public class StatsController extends AbstractFrameController {

	private static final String REMOVED_SCREEN_EVENT_TEXT = "Removed Screen Event";
	private static final String MODIFIED_SCREEN_EVENT_TEXT = "Modified Screen Properties";
	
	private StatsAppFrame statsAppFrame;
	private ModifyScreenEventFrame modifyScreenEventFrame;
	private ScreenEventService screenEventService;
	// Prevents multiple registeration of Action Listeners
	private boolean isRegistered = false;

	@Autowired
	public StatsController(StatsAppFrame statsAppFrame, ModifyScreenEventFrame modifyScreenEventFrame, ScreenEventService screenEventService) {
		this.statsAppFrame = statsAppFrame;
		this.modifyScreenEventFrame = modifyScreenEventFrame;
		this.screenEventService = screenEventService;
	}


	@Override
	public void prepareAndOpenFrame() {
		if (!isRegistered) {
			// CourtPanel Actions
			registerAction(statsAppFrame.getStatsCourtPanel().getElbowLeftButton(), e -> statsAppFrame.ScreenButtonAction(ScreenPosition.ELBOW_LEFT));
			registerAction(statsAppFrame.getStatsCourtPanel().getElbowRightButton(), e -> statsAppFrame.ScreenButtonAction(ScreenPosition.ELBOW_RIGTH));
			registerAction(statsAppFrame.getStatsCourtPanel().getMiddleButton(), e -> statsAppFrame.ScreenButtonAction(ScreenPosition.MIDDLE));
			registerAction(statsAppFrame.getStatsCourtPanel().getSideLeftButton(), e -> statsAppFrame.ScreenButtonAction(ScreenPosition.SIDE_LEFT));
			registerAction(statsAppFrame.getStatsCourtPanel().getSideRightButton(), e -> statsAppFrame.ScreenButtonAction(ScreenPosition.SIDE_RIGHT));
			registerAction(statsAppFrame.getStatsCourtPanel().getFlipCourtButton(), e -> flipCourtEvent());

			// EnterStatsPanel Actions
			registerAction(statsAppFrame.getEnterStatsPanel().getDiscardButton(), (e) -> discardButtonEvent());
			registerActivityPanelButtonActions();
			
			
			isRegistered = true;			
		}
		// Open StatsFrame
		openStatsFrame();
	}

	private void registerActivityPanelButtonActions() {		

		modifyScreenEventFrame.getRemoveButton().addActionListener(e -> {
			removeSelectedScreenEvent();
			statsAppFrame.getEnterStatsPanel().getStatsInfoLabel().setRemoved(REMOVED_SCREEN_EVENT_TEXT);
			modifyScreenEventFrame.dispose();
			
		});
		
		modifyScreenEventFrame.getApplyButton().addActionListener(e -> {
			int removedIndex = removeSelectedScreenEvent();
			addModifiedScreenEvent(removedIndex);
			statsAppFrame.getEnterStatsPanel().getStatsInfoLabel().setModified(MODIFIED_SCREEN_EVENT_TEXT);
			modifyScreenEventFrame.dispose();
		});
		
		modifyScreenEventFrame.getCancelButton().addActionListener(e -> {
			modifyScreenEventFrame.dispose();
		});
		
	}
	
	private void addModifiedScreenEvent(int removedIndex) {
		ScreenEvent screenEvent = modifyScreenEventFrame.getScreenEventFromFields();
		screenEventService.save(screenEvent);
		statsAppFrame.getActivityPanel().getActivityListModel().add(removedIndex, screenEvent);
	}


	public void setTeamNamesAndDate(String ownTeamName, String teamAgainstName, Date gameDate, Optional<HomeAwayEnum> homeAway) {
		getStatsAppFrame().getMatchInfoPanel().setGameInfo(ownTeamName, teamAgainstName, gameDate, homeAway);
		getStatsAppFrame().getEnterStatsPanel().setGameInfo(ownTeamName, teamAgainstName, gameDate, homeAway);
		// Tätä ei oikeestaan tartte koska modifyFramessa haetaan listasta itse screenEvent ja sieltä haetaan myös nämä arvot
		modifyScreenEventFrame.setGameInfo(ownTeamName, teamAgainstName, gameDate, homeAway);
	}
	
	private int removeSelectedScreenEvent() {
		JList<ScreenEvent> list = statsAppFrame.getActivityPanel().getActivityList();
		int removedIndex = list.getSelectedIndex();
		screenEventService.remove(list.getSelectedValue().getId());
		statsAppFrame.getActivityPanel().getActivityListModel().remove(list.getSelectedIndex());
		return removedIndex;
	}

	private void flipCourtEvent() {
		this.statsAppFrame.getStatsCourtPanel().flipCourtAction();
	}

	private void discardButtonEvent() {
		this.statsAppFrame.getEnterStatsPanel().discardEvent();
	}

	private void openStatsFrame() {
		statsAppFrame.setIsOpen(true);
		statsAppFrame.setVisible(true);
	}

	public StatsAppFrame getStatsAppFrame() {
		return statsAppFrame;
	}
}
