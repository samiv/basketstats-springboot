package fi.statsapp.basketstats.business.ui.results;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import fi.statsapp.basketstats.business.entity.ScreenEvent;

public class ResultsListCellRenderer extends JLabel implements ListCellRenderer<ScreenEvent> {

	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<? extends ScreenEvent> list, ScreenEvent value, int index,
			boolean isSelected, boolean cellHasFocus) {

		setText(value.toResultsListString());

		if (isSelected)

		{
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		}
		else
		{
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}

		return this;
	}

}
