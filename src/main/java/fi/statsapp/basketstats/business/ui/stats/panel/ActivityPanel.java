package fi.statsapp.basketstats.business.ui.stats.panel;

import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.ui.stats.ActivityListCenderer;
import net.miginfocom.swing.MigLayout;

@Component
public class ActivityPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final String RECENT_ACTIVITY_LABEL_TEXT = "Recent activity:";
	private static final String LIST_TITLE_STRING = "User, Setter...";
	private static final int MAX_LIST_ROWS = 6;
	
	private JList<ScreenEvent> activityList;
	private DefaultListModel<ScreenEvent> activityListModel;
	
	public ActivityPanel() {
		setLayout(new MigLayout());
		this.activityListModel  = new DefaultListModel<ScreenEvent>();
		this.activityList = new JList<>();
		JLabel recentActivityLabel = new JLabel(RECENT_ACTIVITY_LABEL_TEXT);
		recentActivityLabel.setFont(new Font("Dialog", Font.BOLD, 14));
		JLabel titleLabel = new JLabel(LIST_TITLE_STRING);
		activityList = new JList<ScreenEvent>();
		activityList.setFixedCellWidth(500);
		activityList.setFixedCellHeight(25);
		activityList.setModel(activityListModel);
		activityList.setCellRenderer(new ActivityListCenderer());
		activityList.setVisibleRowCount(MAX_LIST_ROWS);
//		add(recentActivityLabel, "wrap");
		add(titleLabel, "wrap");
		add(new JScrollPane(activityList));
	}

	public JList<ScreenEvent> getActivityList() {
		return activityList;
	}
	
	public DefaultListModel<ScreenEvent> getActivityListModel() {
		return activityListModel;
	}

	public void addScreenEvent(ScreenEvent screenEventToSave) {
		if(activityListModel.size() >= MAX_LIST_ROWS) {
			activityListModel.removeElementAt(MAX_LIST_ROWS-1);		
		}
		activityListModel.add(0, screenEventToSave);
		activityList.setSelectedIndex(0);
	}
	
}
