package fi.statsapp.basketstats.business.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;
import fi.statsapp.basketstats.business.repository.ScreenEventRepository;

@Service
public class ScreenEventService {

	private ScreenEventRepository screenRepository;

	@Autowired
	public ScreenEventService(ScreenEventRepository screenEventRepository) {
		this.screenRepository = screenEventRepository;
	}

	public List<ScreenEvent> findAll() {
		return screenRepository.findAll();
	}

	public void remove(ScreenEvent screenEvent) {
		screenRepository.delete(screenEvent);
	}
	
	public void remove(Long id) {
		screenRepository.deleteById(id);
	}

	public void save(ScreenEvent screenEvent) {
		screenRepository.save(screenEvent);
	}
	
	public List<ScreenEvent> findStatResultsFromRepository(
			HomeAwayEnum homeAway,
			String ownTeamString,
			String teamAgainstString,
			Date gameDate,
			ScreenPosition pos,
			Integer user,
			Integer setter,
			ScreenDirection dir,
			ScreenDefence def,
			ScreenEnding ending,
			ScreenShotClock clock) {
		return screenRepository.findStatsResults(homeAway, ownTeamString, teamAgainstString, gameDate, pos, user, setter, dir, ending, def, clock);
	}

}
