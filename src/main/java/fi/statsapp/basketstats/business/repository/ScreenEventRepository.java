package fi.statsapp.basketstats.business.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fi.statsapp.basketstats.business.entity.ScreenEvent;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;

@Repository
public interface ScreenEventRepository extends JpaRepository<ScreenEvent, Long> {

	@Query("select u from ScreenEvent u where (:homeAway is null or u.homeAway = :homeAway)"
			+ " and (:ownTeamString is null or u.ownTeamString = :ownTeamString)"
			+ " and (:teamAgainstString is null or u.teamAgainstString = :teamAgainstString)"
			+ " and (:gameDate is null or u.gameDate = :gameDate)"
			+ " and (:position is null or u.position = :position)"
			+ " and (:user_jersey is null or u.user_jersey = :user_jersey)"
			+ " and (:setter_jersey is null or u.setter_jersey = :setter_jersey)"
			+ " and (:direction is null or u.direction = :direction)"
			+ " and (:ending is null or u.ending = :ending)"
			+ " and (:defence is null or u.defence = :defence)"
			+ " and (:clock is null or u.clock = :clock)")
	List<ScreenEvent> findStatsResults(
			@Param("homeAway") HomeAwayEnum homeAway,
			@Param("ownTeamString") String ownTeamString,
			@Param("teamAgainstString") String teamAgainstString,
			@Param("gameDate") Date gameDate,
			@Param("position") ScreenPosition position,
			@Param("user_jersey") Integer user_jersey,
			@Param("setter_jersey") Integer setter_jersey,
			@Param("direction") ScreenDirection direction, 
			@Param("ending") ScreenEnding ending,
			@Param("defence") ScreenDefence defence,
			@Param("clock") ScreenShotClock clock);
	
	@Transactional
	void deleteById(Long id);

}
