package fi.statsapp.basketstats.business.player;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "player")
public class Player implements Comparable<Player> {

	@Element(name = "playerName")
	private String name;
	@Element(name = "jersey")
	private Integer jersey;
	
	private boolean selected = false;

	private static final Character PLAYER_SEPARATOR = ',';

	public Player(
			@Element(name = "playerName") String name,
			@Element(name = "jersey") Integer jersey) {
		this.name = name;
		this.jersey = jersey;
	}
	

	public String getName() {
		return this.name;
	}

	public Integer getJersey() {
		return this.jersey;
	}

	public String toString() {
		if (this.jersey == null && this.name == null) {
			return "";
		} else {
			String result = "#" + this.jersey + " " + this.name;
			return result;
		}

	}

	public static Player stringToPlayer(String playerString) {
		int jerseyCharIdx = playerString.lastIndexOf(PLAYER_SEPARATOR);
		String playerName = playerString.substring(jerseyCharIdx+2);
		int playerJersey = Integer.parseInt(playerString.substring(1, jerseyCharIdx));

		return new Player(playerName, playerJersey);
	}

	public static String findJerseyString(String playerString) {
		int jerseyCharIdx = playerString.lastIndexOf(PLAYER_SEPARATOR);
		String jersey = playerString.substring(1, jerseyCharIdx);
		return jersey;		
	}

	public static String findPlayerNameString(String playerString) {
		int jerseyCharIdx = playerString.lastIndexOf(PLAYER_SEPARATOR);
		String playerName = playerString.substring(jerseyCharIdx+2);
		return playerName;
	}

	@Override
	public boolean equals(Object other) {
		if(!(other instanceof Player)) {
			return false;
		}
		Player otherPlayer = (Player) other;
		return name == otherPlayer.name && jersey == otherPlayer.jersey;
	}

	@Override
	public int hashCode() {
		return (jersey + 10)*3 + 4*(5 + jersey);
	}

	@Override
	public int compareTo(Player other) {
		if (jersey < other.jersey) {
			return -1;
		}
		return jersey == other.jersey ? 0 : 1;
	}

	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
