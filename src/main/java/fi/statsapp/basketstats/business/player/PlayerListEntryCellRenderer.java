package fi.statsapp.basketstats.business.player;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class PlayerListEntryCellRenderer implements ListCellRenderer<Object> {

	// TODO: oisko helpompi extendata JList niinkuin ResultsCellRendererissa?
	
	private final String SELECTED = "icons\\selected.png";
	private final String DESELECTED = "icons\\deselected.png";
	private JLabel label;
	
	public PlayerListEntryCellRenderer() {
		label = new JLabel();
		label.setOpaque(true);
	}
	
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {	
		
		Player entry = (Player) value;
		  
	      label.setText(value.toString());
	      if (entry.getSelected()) {
	    	  label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(SELECTED)));
	      } else {
	    	  label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(DESELECTED)));
	      }
	      
	   
	      if (isSelected) {
	         label.setBackground(Color.BLUE);
	         label.setForeground(Color.ORANGE);
	      }
	      else {
	    	  label.setBackground(list.getBackground());
	    	  label.setForeground(list.getForeground());
	      }
	  
	      label.setEnabled(list.isEnabled());
	      label.setFont(list.getFont());
	  
	      return label;
	}

}
