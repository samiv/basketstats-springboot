package fi.statsapp.basketstats.business.player;

import javax.swing.JPanel;
import javax.swing.JRadioButton;

import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;

public class HomeAwayRadioButtonPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JRadioButton homeRadioButton;
	private JRadioButton awayRadioButton;
	
	public HomeAwayRadioButtonPanel() {
		createRadioButtons();
	}

	private void createRadioButtons() {
		homeRadioButton = new JRadioButton(HomeAwayEnum.HOME.toString());
		awayRadioButton = new JRadioButton(HomeAwayEnum.AWAY.toString());
		
		homeRadioButton.addActionListener( (e) -> {
			homeRadioButton.setSelected((homeRadioButton.isSelected()));
			awayRadioButton.setSelected(false);
		});
		
		awayRadioButton.addActionListener( (e) -> {
			awayRadioButton.setSelected((awayRadioButton.isSelected()));
			homeRadioButton.setSelected(false);
		});
		add(homeRadioButton);
		add(awayRadioButton);
	}
	
	public HomeAwayEnum getSelectedHomeAway() {
		if(homeRadioButton.isSelected()) {
			return HomeAwayEnum.HOME;
		} else if (awayRadioButton.isSelected()) {
			return HomeAwayEnum.AWAY;
		} else {
			return null;
		}
	}
}
