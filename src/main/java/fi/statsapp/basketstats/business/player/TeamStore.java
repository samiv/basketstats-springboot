package fi.statsapp.basketstats.business.player;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "teamStore")
public class TeamStore {

	@Element(name = "lastOwnTeam")
	private String lastOwnTeam;

	@ElementList(name = "teamList")
	private final List<Team> teamList;

	public TeamStore(
			@Element(name = "lastOwnTeam") String lastOwnTeam,
			@ElementList(name = "teamList") List<Team> teamList) {
		this.teamList = teamList;
		this.lastOwnTeam = lastOwnTeam;
	}

	public List<Team> getTeamList() {
		return teamList;
	}
	
	public String getLastOwnTeam() {
		return lastOwnTeam;
	}

	public void setLastOwnTeam(String lastOwnTeam) {
		this.lastOwnTeam = lastOwnTeam;
	}
	
	public void addTeam(Team teamToAdd) {
		teamList.add(teamToAdd);
	}

	public void removeTeam(Team teamToRemove) {
		teamList.remove(teamToRemove);
	}

	public Team findTeamByName(String string) {
		for(Team team : teamList) {
			if(team.getTeamName().equalsIgnoreCase(string)) {
				return team;
			}
		}
		// LOG.warn("No team find from teamList, {}", team.getTeamName());
		return null;
	}

	public Team getTeam(String name) {

		for (Team team : teamList) {
			if (team.getTeamName().equalsIgnoreCase(name)) {
				return team;
			}
		}
		return null;
	}
}
