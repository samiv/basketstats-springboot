package fi.statsapp.basketstats.business.player;

import java.util.Collections;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "team")
public class Team implements Comparable<Team>{

	@Attribute(name = "teamName")
	private final String teamName;
	
	@ElementList(name = "playerList")
	private final List<Player> playerList;
	
	public Team(
			@Attribute(name = "teamName") String teamName,
			@ElementList(name = "playerList") List<Player> playerList) {
		this.teamName = teamName;
		this.playerList = playerList;
	}

	public String getTeamName() {
		return teamName;
	}

	public List<Player> getPlayerList() {
		return playerList;
	}
	
	public void addPlayer(Player playerToAdd) {
		playerList.add(playerToAdd);
		Collections.sort(playerList);
	}
	
	public void removePlayer(Player playerToRemove) {
		playerList.remove(playerToRemove);
	}
	
	@Override
	public int compareTo(Team other) {
		return this.teamName.compareTo(other.teamName);
	}

	
	@Override
	public String toString() {
		if(teamName != null) {
			return teamName;
		} else {
			return "";
		}
	}
}
