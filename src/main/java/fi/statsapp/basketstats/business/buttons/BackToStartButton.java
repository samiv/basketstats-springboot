package fi.statsapp.basketstats.business.buttons;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

public class BackToStartButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BackToStartButton() {
		setText("<<< Back");
		setBackground(Color.GREEN);
		setPreferredSize(new Dimension(100, 30));
		
	}
	
}
