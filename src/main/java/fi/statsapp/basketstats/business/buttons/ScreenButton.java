package fi.statsapp.basketstats.business.buttons;

import java.awt.Dimension;

import javax.swing.JButton;

public class ScreenButton extends JButton {
	
	private static final long serialVersionUID = 1L;

	public ScreenButton(String name) {
		setText(name);
		setPreferredSize(new Dimension(50, 50));
	}
}
