package fi.statsapp.basketstats.business.enums.screens;

public enum ScreenShotClock implements ScreenProperty {
	// kutsutaan ShotClockTime.EARLY.nameText()
	EARLY(EARLY_TXT, "0-8 sec", 'E'),
	MEDIUM(MEDIUM_TXT, "8-16 sec", 'M'),
	LATE(LATE_TXT, "16-24 sec", 'L');

	private String nameText;
	private String timeText;
	private Character key;

	ScreenShotClock(String nameText, String timeText, Character key){
		this.nameText = nameText;
		this.timeText = timeText;
		this.key = key;
	}

	public String getNameText() {
		return this.nameText;
	}

	public String getTimeText() {
		return this.timeText;
	}

	@Override
	public Character getKey() {
		return this.key;
	}

	public static ScreenShotClock returnTimeFromChar(Character key) {
		if(key == ScreenShotClock.EARLY.getKey()) {
			return ScreenShotClock.EARLY;
		} else if(key == ScreenShotClock.MEDIUM.getKey()) {
			return ScreenShotClock.MEDIUM;
		} else if(key == ScreenShotClock.LATE.getKey()) {
			return ScreenShotClock.LATE;
		} else {
			System.out.println("Cannot convert Clock char to enum");
			return null;
		}
	}

	@Override
	public String getValue() {
		return this.nameText;
	}

	public static ScreenShotClock returnShotClockFromString(String clock) {
		switch (clock) {
		case EARLY_TXT:
			return EARLY;
		case MEDIUM_TXT:
			return MEDIUM;
		case LATE_TXT:
			return LATE;
		default:
			return null;
		}
	}
}
