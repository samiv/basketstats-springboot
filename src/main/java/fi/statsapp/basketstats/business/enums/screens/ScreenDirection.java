package fi.statsapp.basketstats.business.enums.screens;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ScreenDirection implements ScreenProperty {
	CENTER(TO_CENTER_TXT, 'C'),
	SIDE(TO_SIDE_TXT, 'S');
	
	private static final Logger LOG = LoggerFactory.getLogger(ScreenDirection.class);
	
	private String value;
	private Character key;
	
	ScreenDirection(String value, Character key) {
		this.value = value;
		this.key = key;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public Character getKey() {
		return this.key;
	}
	
	@Override
	public String toString() {
		return value;
		
	}
	
	public static ScreenDirection returnDirFromChar(Character key) {
		if(key == ScreenDirection.CENTER.getKey()) {
			return ScreenDirection.CENTER;
		} else if (key == ScreenDirection.SIDE.getKey()) {
			return ScreenDirection.SIDE;
		} else {
			LOG.info("Cannot convert Direction char to enum");
			return null;
		}
		
		
	}
	
	public static ScreenDirection returnDirectionFromString(String dir) {
		switch (dir) {
		case TO_CENTER_TXT:
			return CENTER;
		case TO_SIDE_TXT:
			return SIDE;
		default:
			return null;
		
		}
	}
}
