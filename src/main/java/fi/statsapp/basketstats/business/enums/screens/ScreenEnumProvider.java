package fi.statsapp.basketstats.business.enums.screens;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.ui.stats.panel.EnumPanel;

@Component
public class ScreenEnumProvider {
	
	private static final Logger LOG = LoggerFactory.getLogger(ScreenEnumProvider.class);

	private JScrollPane shotClockOptionsScroll;
	private JScrollPane defenceOptionsScroll;
	private JScrollPane desicionOptionsScroll;
	private JScrollPane directionOptionsScroll;

	private JTable table;
	private DefaultTableModel optionsTableModel;
	private String[] columns = new String[] {
			"Key", "Value"
	};
	private List<Player> playerList;

	public ScreenEnumProvider() {

		createShotClockScroll();
		createDefenceOptionsScroll();
		createDesicionOptionsScroll();
		createDirectionOptionsScroll();
		createPlayerTable();
	}

	private void createPlayerTable() {

		optionsTableModel = new DefaultTableModel(0, 0);
		table = new JTable(optionsTableModel);	
		optionsTableModel.setColumnIdentifiers(columns);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setEnabled(false);
		table.setFillsViewportHeight(true);

		DefaultTableCellRenderer cr = new DefaultTableCellRenderer();
		cr.setHorizontalAlignment(JLabel.CENTER);

		for(int k = 0; k < columns.length; ++k) {
			TableColumn a = table.getColumnModel().getColumn(k);
			if ( k == 0) {
				a.setPreferredWidth(35);
				a.setCellRenderer(cr);
			} else if (k == 1) {
				a.setPreferredWidth(150);
			}
		}
	}

	public JScrollPane getPlayerListScroll() {
		JPanel playerListPanel = new JPanel();
		playerListPanel.add(this.table);
		JScrollPane playerScrollPanel = new JScrollPane(playerListPanel);
		playerScrollPanel.setPreferredSize(new OptionsPanelDimension());
		return playerScrollPanel;

	}

	// Tätä kutsutaan SetPlayersin next nappulassa
	public void updatePlayerTable(List<Player> updatedPlayersList ) {
		optionsTableModel = new DefaultTableModel(0, 0);
		table.setModel(optionsTableModel);
		optionsTableModel.setColumnIdentifiers(columns);
		playerList = updatedPlayersList;
		for(Player player : updatedPlayersList) {
			optionsTableModel.addRow(new Object[] {player.getJersey(), player.getName()});
		}
	}

	private void createDirectionOptionsScroll() {
		String[] directionArray = new String[ScreenDirection.values().length];
		char[] directionKeysArray = new char[ScreenDirection.values().length];
		int s = 0;
		for(ScreenDirection direction : ScreenDirection.values()) {
			directionArray[s] = direction.getValue();
			directionKeysArray[s] = direction.getKey();
			++s;
		}

		this.directionOptionsScroll = new JScrollPane(new EnumPanel(directionKeysArray, directionArray));
		this.directionOptionsScroll.setPreferredSize(new OptionsPanelDimension());

	}

	private void createDesicionOptionsScroll() {
		String[] desicionArray = new String[ScreenEnding.values().length];
		char[] desicionKeysArray = new char[ScreenEnding.values().length];
		int k = 0;
		for(ScreenEnding desicion : ScreenEnding.values()) {
			desicionArray[k] = desicion.getValue();
			desicionKeysArray[k] = desicion.getKey();
			++k;
		}

		this.desicionOptionsScroll= new JScrollPane(new EnumPanel(desicionKeysArray, desicionArray));
		this.desicionOptionsScroll.setPreferredSize(new OptionsPanelDimension());

	}

	private void createDefenceOptionsScroll() {
		String[] defenceArray = new String[ScreenDefence.values().length];
		char[] defenceKeysArray = new char[ScreenDefence.values().length];
		int j = 0;
		for(ScreenDefence defence : ScreenDefence.values()) {
			defenceArray[j] = defence.getValue();
			defenceKeysArray[j] = defence.getKey();
			++j;
		}

		this.defenceOptionsScroll = new JScrollPane(new EnumPanel(defenceKeysArray, defenceArray));
		this.defenceOptionsScroll.setPreferredSize(new OptionsPanelDimension());

	}

	private void createShotClockScroll() {
		String[] shotClockArray = new String[ScreenShotClock.values().length];
		char[] shotClockKeysArray = new char[ScreenShotClock.values().length];
		int i = 0;
		for(ScreenShotClock shotClock : ScreenShotClock.values()) {
			shotClockArray[i] = shotClock.getValue() + " (" + shotClock.getTimeText() + ")";
			shotClockKeysArray[i] = shotClock.getKey();
			++i;
		}

		this.shotClockOptionsScroll = new JScrollPane(new EnumPanel(shotClockKeysArray, shotClockArray));
		this.shotClockOptionsScroll.setPreferredSize(new OptionsPanelDimension());

	}


	public JScrollPane getShotClockOptionsScroll() {
		return shotClockOptionsScroll;
	}

	public JScrollPane getDefenceOptionsScroll() {
		return defenceOptionsScroll;
	}

	public JScrollPane getDesicionOptionsScroll() {
		return desicionOptionsScroll;
	}

	public JScrollPane getDirectionOptionsScroll() {
		return directionOptionsScroll;
	}

	public List<Player> getPlayerList() {
		if(playerList != null) {
			return playerList;
		}
		else {
			return new ArrayList<Player>();
		}
	}
	
	public String getPlayerName(Integer jerseyToFind) {
		for(Player player : playerList) {
			if(player.getJersey() == jerseyToFind) {
				return player.getName();
			}
		}
		LOG.warn("Player not found when converting playerJersey to playerName when committing event (or getPlayerName is called for some other reason...)");
		return null;
	}




}
