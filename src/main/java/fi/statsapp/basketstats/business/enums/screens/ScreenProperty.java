package fi.statsapp.basketstats.business.enums.screens;

public interface ScreenProperty {

	String getValue();
	Character getKey();
	
	// ScreenTexts
	
	//Defence
	static final String HARD_HEDGE_TXT = "Hard hedge";
	static final String SOFT_HEDGE_TXT = "Soft hedge";
	static final String OVER_OVER_TXT = "Over & over";
	static final String OVER_UNDER_TXT = "Over & under";
	static final String SOFT_UNDER_TXT = "Soft & under";
	static final String SOFT_OVER_TXT = "Soft & over";
	static final String PUSH_UNDER_TXT = "Push & under";
	static final String GAP_UNDER_TXT = "Gap & under";
	
	// Direction
	static final String TO_SIDE_TXT = "To side";
	static final String TO_CENTER_TXT = "To center";
	
	// Ending
	static final String SHOT_TXT = "Shot";
	static final String PASS_TXT = "Pass";
	static final String PENETRATION_TXT = "Penetration";
	
	// Position
	static final String MIDDLE_TXT = "Middle";
	static final String ELBOW_LEFT_TXT = "Elbow L";
	static final String SIDE_LEFT_TXT = "Side L";
	static final String ELBOW_RIGHT_TXT = "Elbow R";
	static final String SIDE_RIGHT_TXT = "Side R";
	
	// Shot clock
	static final String EARLY_TXT = "Early";
	static final String MEDIUM_TXT = "Medium";
	static final String LATE_TXT = "Late";
	
	// HOME/AWAY
	static final String HOME_TXT = "HOME";
	static final String AWAY_TXT = "AWAY";
	
}
