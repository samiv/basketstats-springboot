package fi.statsapp.basketstats.business.enums.screens;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ScreenPosition implements ScreenProperty {

	MIDDLE (MIDDLE_TXT),
	ELBOW_LEFT (ELBOW_LEFT_TXT),
	SIDE_LEFT (SIDE_LEFT_TXT),
	ELBOW_RIGTH (ELBOW_RIGHT_TXT),
	SIDE_RIGHT (SIDE_RIGHT_TXT);

	private static final Logger LOG = LoggerFactory.getLogger(ScreenPosition.class);

	private final String name;

	private ScreenPosition(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String toString() {
		return this.name;
	}

	public static ArrayList<String> getScreenPositionStrings() {
		ArrayList<String> strings = new ArrayList<String>();
		for (ScreenPosition sp : ScreenPosition.values()) {
			strings.add(sp.getValue());
		}
		return strings;
	}

	public static ScreenPosition returnPosFromString(String key) {
		if (key == null) {
			return null;
		} else if(key.equals(ScreenPosition.MIDDLE.getValue())) {
			return ScreenPosition.MIDDLE;
		} else if(key.equals(ScreenPosition.ELBOW_LEFT.getValue())) {
			return ScreenPosition.ELBOW_LEFT;
		} else if(key.equals(ScreenPosition.SIDE_LEFT.getValue())) {
			return ScreenPosition.SIDE_LEFT;
		} else if(key.equals(ScreenPosition.ELBOW_RIGTH.getValue())) {
			return ScreenPosition.ELBOW_RIGTH;
		} else if(key.equals(ScreenPosition.SIDE_RIGHT.getValue())) {
			return ScreenPosition.SIDE_RIGHT;
		} else {
			LOG.info("Cannot convert Position char to enum, Returning null");
			return null;
		}
	}

	@Override
	public String getValue() {
		return this.name;
	}

	@Override
	public Character getKey() {
		LOG.warn("ScreenPosition HAS NOT KEY, SOMEWHERE SCREENPOSITION.GETKEY() is invoked!!");
		return 0;
	}
}
