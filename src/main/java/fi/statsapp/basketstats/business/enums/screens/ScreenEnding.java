package fi.statsapp.basketstats.business.enums.screens;

public enum ScreenEnding implements ScreenProperty {
	SHOT(SHOT_TXT, 'S'),
	PASS(PASS_TXT, 'P'),
	PENETRATION(PENETRATION_TXT, 'P');

	private String value;
	private Character key;

	ScreenEnding(String value, Character key) {
		this.value = value;
		this.key = key;
	}

	public String getValue() {
		return this.value;
	}

	public Character getKey() {
		return this.key;
	}

	public static ScreenEnding returnDecFromChar(Character key) {
		if(key == ScreenEnding.SHOT.getKey()) {
			return ScreenEnding.SHOT;
		} else if(key == ScreenEnding.PASS.getKey()) {
			return ScreenEnding.PASS;
		} else if(key == ScreenEnding.PENETRATION.getKey()) {
			return ScreenEnding.PENETRATION;
		} else {
			System.out.println("Cannot convert Ending char to enum");
			return null;
		}
	}

	public static ScreenEnding returnEndingFromString(String ending) {
		switch (ending) {
		case SHOT_TXT:
			return SHOT;
		case PASS_TXT:
			return PASS;
		case PENETRATION_TXT:
			return PENETRATION;
		default:
			return null;
		}
	}
}
