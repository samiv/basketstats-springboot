package fi.statsapp.basketstats.business.enums.screens;

public enum ScreenDefence implements ScreenProperty {	
	
	HARD_HEDGE(HARD_HEDGE_TXT,'H'),
	SOFT_HEDGE(SOFT_HEDGE_TXT,'S'),
	OVER_OVER(OVER_OVER_TXT,'O'),
	OVER_UNDER(OVER_UNDER_TXT,'U'),
	SOFT_UNDER(SOFT_UNDER_TXT,'D'),
	SOFT_OVER(SOFT_OVER_TXT,'F'),
	PUSH_UNDER(PUSH_UNDER_TXT,'P'),
	GAP_UNDER(GAP_UNDER_TXT,'G');
	
	private String value;
	private char key;

	ScreenDefence(String value, Character key) {
		this.value = value;
		this.key = key;
	}

	public String getValue() {
		return this.value;
	}

	public Character getKey() {
		return this.key;
	}

	public static ScreenDefence returnDefFromChar(Character key) {
		
		if(key == ScreenDefence.HARD_HEDGE.getKey()) {
			return ScreenDefence.HARD_HEDGE;
		} else if(key == ScreenDefence.SOFT_HEDGE.getKey()) {
			return ScreenDefence.SOFT_HEDGE;
		} else if(key == ScreenDefence.OVER_OVER.getKey()) {
			return ScreenDefence.OVER_OVER;
		} else if(key == ScreenDefence.OVER_UNDER.getKey()) {
			return ScreenDefence.OVER_UNDER;
		} else if(key == ScreenDefence.SOFT_UNDER.getKey()) {
			return ScreenDefence.SOFT_UNDER;
		} else if(key == ScreenDefence.SOFT_OVER.getKey()) {
			return ScreenDefence.SOFT_OVER;
		} else if(key == ScreenDefence.PUSH_UNDER.getKey()) {
			return ScreenDefence.PUSH_UNDER;
		} else if(key == ScreenDefence.GAP_UNDER.getKey()) {
			return ScreenDefence.GAP_UNDER;
		}
		else {
			System.out.println("Cannot convert Defence char to enum");
			return null;
		}

	}

	public static ScreenDefence returnDefenceFromString(String def) {
		switch (def) {
		case HARD_HEDGE_TXT:
			return HARD_HEDGE;
		case SOFT_HEDGE_TXT:
			return SOFT_HEDGE;
		case OVER_OVER_TXT:
			return OVER_OVER;
		case OVER_UNDER_TXT:
			return OVER_UNDER;
		case SOFT_UNDER_TXT:
			return SOFT_UNDER;
		case SOFT_OVER_TXT:
			return SOFT_OVER;
		case PUSH_UNDER_TXT:
			return PUSH_UNDER;
		case GAP_UNDER_TXT:
			return GAP_UNDER;
		default:
			return null;
		}
	}

}




