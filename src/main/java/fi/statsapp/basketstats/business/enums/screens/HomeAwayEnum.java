package fi.statsapp.basketstats.business.enums.screens;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum HomeAwayEnum implements ScreenProperty {
	HOME(HOME_TXT),
	AWAY(AWAY_TXT);
	
	private static final Logger LOG = LoggerFactory.getLogger(HomeAwayEnum.class);
	
	private String text;
	
	private HomeAwayEnum(String text) {
		this.text = text;
	}
	
	public String toString() {
		return this.text;
	}

	@Override
	public String getValue() {
		
		return null;
	}

	@Override
	public Character getKey() {
		LOG.warn("HomeAwayEnum HAS NOT KEY, SOMEWHERE HOMEAWAY.GETKEY() is invoked!!");
		return 0;
	}

}
