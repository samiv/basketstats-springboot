package fi.statsapp.basketstats.business.enums.screens;

import java.awt.Dimension;

public class OptionsPanelDimension extends Dimension {
	
	private static final long serialVersionUID = 1L;

	public OptionsPanelDimension() {
		super(350, 300);
	}
}
