package fi.statsapp.basketstats.business.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

import javax.persistence.*;

import com.google.common.base.MoreObjects;

import fi.statsapp.basketstats.StatEvent;
import fi.statsapp.basketstats.business.enums.screens.ScreenDirection;
import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenDefence;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnding;
import fi.statsapp.basketstats.business.enums.screens.ScreenPosition;
import fi.statsapp.basketstats.business.enums.screens.ScreenShotClock;

@Entity
@Table(name = "screen_event")
public class ScreenEvent implements StatEvent, Serializable {

	private static final long serialVersionUID = 1L;

	private static final String NO_STAT_STRING_TO_RESULTS_LIST = "--";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "homeAway")
	private HomeAwayEnum homeAway;
	
	@Column(name = "ownTeam")
	private String ownTeamString;
	
	@Column(name = "teamAgainst")
	private String teamAgainstString;
	
	@Column(name = "gameDate")
	private Date gameDate;

	@Column(name = "user_jersey")	
	private Integer user_jersey;

	@Column(name = "user_name")	
	private String user_name;

	@Column(name = "setter_jersey")	
	private Integer setter_jersey;

	@Column(name = "setter_name")	
	private String setter_name;

	@Enumerated(EnumType.STRING)
	@Column(name = "position")	
	private ScreenPosition position;

	@Enumerated(EnumType.STRING)
	@Column(name = "direction")	
	private ScreenDirection direction;

	@Enumerated(EnumType.STRING)
	@Column(name = "ending")	
	private ScreenEnding ending;

	
	@Enumerated(EnumType.STRING)
	@Column(name = "defence")
	private ScreenDefence defence;

	@Enumerated(EnumType.STRING)
	@Column(name = "clock")
	private ScreenShotClock clock;

	public ScreenEvent() {
	}

	public ScreenEvent(
			HomeAwayEnum homeAway,
			String ownTeamString,
			String teamAgainstString,
			Date gameDate,
			ScreenPosition pos,
			Integer userJersey,
			String userName,
			Integer setterJersey,
			String setterName,
			ScreenDirection direction,
			ScreenDefence def,
			ScreenEnding end,
			ScreenShotClock clock) {
		this.homeAway = homeAway;
		this.ownTeamString = ownTeamString;
		this.teamAgainstString = teamAgainstString;
		this.gameDate = gameDate;
		this.position = pos;
		this.user_jersey = userJersey;
		this.user_name = userName;
		this.setter_jersey = setterJersey;
		this.setter_name = setterName;
		this.direction = direction;
		this.ending = end;
		this.defence = def;
		this.clock = clock;
	}

	// TODO: Immutableksi?? eli setterit pois. Tässä olis se builder-juttu, mutta en tiiä toimiiko tässä DataBase-jutussa
	// Builderilla voi luoda uuden instanssin jossa ei kaikkia kenttiä. Builderilla on setMetodit. Kun on kutsuttu build -> rakennetaan
	// instanssi ja sen jälkeen ei voida enää kutsua setMetodeja, koska tämä luokka on immutable.


	public Long getId() {
		return id;
	}
	
	public Optional<HomeAwayEnum> getHomeAway() {
		return Optional.ofNullable(homeAway);
	}

	public Date getGameDate() {
		return gameDate;
	}

	public String getOwnTeamString() {
		return ownTeamString;
	}

	public String getTeamAgainstString() {
		return teamAgainstString;
	}
	
	public String getUserPlayerName() {
		return user_name;
	}

	public String getSetterPlayerName() {
		return setter_name;
	}

	public Integer getUserPlayerJersey() {
		return user_jersey;
	}

	public Integer getSetterPlayerJersey() {
		return setter_jersey;
	}

	public ScreenPosition getPosition() {
		return position;
	}

	public ScreenDirection getDirection() {
		return direction;
	}

	public ScreenEnding getEnding() {
		return ending;
	}

	public ScreenDefence getDefenceStyle() {
		return defence;
	}

	public ScreenShotClock getShotClock() {
		return clock;
	}

	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("ID", id)
				.add("position", position == null ? NO_STAT_STRING_TO_RESULTS_LIST : position.getValue())
				.add("User#", user_jersey == null ? NO_STAT_STRING_TO_RESULTS_LIST : user_jersey)
				.add("User name", user_name == null ? NO_STAT_STRING_TO_RESULTS_LIST : user_name)
				.add("Setter#", setter_jersey == null ? NO_STAT_STRING_TO_RESULTS_LIST : setter_jersey)
				.add("Setter name", setter_name == null ? NO_STAT_STRING_TO_RESULTS_LIST : setter_name)
				.add("Direction", direction == null ? NO_STAT_STRING_TO_RESULTS_LIST : direction.getValue())
				.add("Ending", ending == null ? NO_STAT_STRING_TO_RESULTS_LIST : ending.getValue())
				.add("Defence", defence == null ? NO_STAT_STRING_TO_RESULTS_LIST : defence.getValue())
				.add("ShotClock", clock == null ? NO_STAT_STRING_TO_RESULTS_LIST : clock.getTimeText())
				.add("Home/Away", homeAway == null ? NO_STAT_STRING_TO_RESULTS_LIST : homeAway)
				.add("Own Team", ownTeamString == null ? NO_STAT_STRING_TO_RESULTS_LIST : ownTeamString)
				.add("Team Against", teamAgainstString == null ? NO_STAT_STRING_TO_RESULTS_LIST : teamAgainstString)
				.add("GameDate", gameDate == null ? NO_STAT_STRING_TO_RESULTS_LIST : gameDate.toString())
				.toString();
	}

	@Override
	public String toRecentListString() {
		// TODO: recentListin synkronointi/esitys järkevästi on täysin miettimättä
		return null;
	}

	@Override
	public String toResultsListString() {
		String userJ = user_jersey == null ? NO_STAT_STRING_TO_RESULTS_LIST : user_jersey.toString();
		String userN = user_name == null ? NO_STAT_STRING_TO_RESULTS_LIST : user_name;
		String setterJ = setter_jersey == null ? NO_STAT_STRING_TO_RESULTS_LIST : setter_jersey.toString();
		String setterN = setter_name == null ? NO_STAT_STRING_TO_RESULTS_LIST : setter_name;
		String pos = position == null ? NO_STAT_STRING_TO_RESULTS_LIST : position.getValue();
		String dire= direction == null ? NO_STAT_STRING_TO_RESULTS_LIST : direction.getValue();
		String end = ending == null ? NO_STAT_STRING_TO_RESULTS_LIST : ending.getValue();
		String defe= defence == null ? NO_STAT_STRING_TO_RESULTS_LIST : defence.getValue();
		String clk= clock == null ? NO_STAT_STRING_TO_RESULTS_LIST : clock.getValue();
		String ha= homeAway == null ? NO_STAT_STRING_TO_RESULTS_LIST : homeAway.toString();
		String ownT= ownTeamString == null ? NO_STAT_STRING_TO_RESULTS_LIST : ownTeamString;
		String againstT= teamAgainstString == null ? NO_STAT_STRING_TO_RESULTS_LIST : teamAgainstString;
		String gDate= gameDate == null ? NO_STAT_STRING_TO_RESULTS_LIST : gameDate.toString();
		
		return "User= #" + userJ + " " + userN + 
				"; Setter= #" + setterJ + " " + setterN
				+ "; Position=" + pos
				+ "; Direction=" + dire
				+ "; Ending=" + end
				+ "; Defence=" + defe
				+ "; Clock=" + clk
				+ "; Home/Away=" + ha
				+ ", OwnTeam=" + ownT
				+ "; TeamAgainst=" + againstT
				+ "; GameDate=" + gDate;
	}
}
