package fi.statsapp.basketstats.main_menu.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.swing.JOptionPane;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fi.statsapp.basketstats.business.enums.screens.HomeAwayEnum;
import fi.statsapp.basketstats.business.enums.screens.ScreenEnumProvider;
import fi.statsapp.basketstats.business.player.Player;
import fi.statsapp.basketstats.business.player.Team;
import fi.statsapp.basketstats.business.player.TeamStore;
import fi.statsapp.basketstats.business.ui.results.ResultsController;
import fi.statsapp.basketstats.business.ui.set_players.SetPlayersController;
import fi.statsapp.basketstats.business.ui.set_players.SetPlayersFrame;
import fi.statsapp.basketstats.business.ui.set_players.panel.AddPlayersPanel;
import fi.statsapp.basketstats.business.ui.set_players.panel.PlayerListPanel;
import fi.statsapp.basketstats.business.ui.shared.controller.AbstractFrameController;
import fi.statsapp.basketstats.business.ui.stats.ModifyScreenEventFrame;
import fi.statsapp.basketstats.business.ui.stats.StatsController;
import fi.statsapp.basketstats.business.ui.stats.panel.ActivityPanel;
import fi.statsapp.basketstats.main_menu.ui.MainMenuFrame;

@Controller
public class MainMenuController extends AbstractFrameController {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainMenuController.class);

	private static final String TEAM_STORE_FILE_PATH = "user.home";

	private static final String SAME_TEAMS_ERROR_TEXT = "Cannot proceed with two equal teams. Select two different teams and try again";
	private static final String SELECT_ATLEAST_TWO_PLAYERS_ERROR_TEXT = "Select at least two players to proceed";
	private static final Object SELECT_TEAMS_ERROR_TEXT = "Select both teams to proceed";
	private static final String ERROR_TEXT = "ERROR";
	
	private MainMenuFrame mainMenuFrame;
	private SetPlayersController setPlayersController;
	private ResultsController resultsController;
	private StatsController statsController;
	private ScreenEnumProvider screenEnumProvider;
	
	private Team ownTeam;
	private String teamAgainstName;
	private Date gameDate;
	private Optional<HomeAwayEnum> homeAway;
	// TODO: Eikö teamStore pitäisi olla kaikilla controllereilla ettei täällä tartteisi aina kysyä ja päivitellä kun tarttetaan teamStorea..
	// Ei esimerkiksi actitvityPaneelin tuplaklikkiä pitäisi rekisteröidä täällä. Täällä vain ne jotka liittyy jotenkin MainMenuun (i.e. MainMenuFrameen)
	private TeamStore teamStore;
	
	private ModifyScreenEventFrame modifyScreenEventFrame;

	@Autowired
	public MainMenuController(MainMenuFrame mainMenuFrame, SetPlayersController setPlayersController,
			ResultsController resultsController, StatsController statsController, ScreenEnumProvider screenEnumProvider,
			ModifyScreenEventFrame modifyScreenEventFrame) {
		this.mainMenuFrame = mainMenuFrame;
		this.setPlayersController = setPlayersController;
		this.resultsController = resultsController;
		this.statsController = statsController;
		this.screenEnumProvider = screenEnumProvider;
		this.modifyScreenEventFrame = modifyScreenEventFrame;
	}

	@Override
	public void prepareAndOpenFrame() {
		mainMenuFrame.setVisible(true);
		// Read team-configuration from XML-file
		readXmlFile(TEAM_STORE_FILE_PATH);
		if(teamStore != null) {
			mainMenuFrame.addSetGameInfoPanel(teamStore);			
		} else {
			LOG.error("TeamStore null after XML-reading");
		}
		LOG.debug("After xml reading");
		// MAIN MENU BUTTONS
		registerAction(mainMenuFrame.getStartButtonsPanel().getSetPlayersPanelBtn(), (e) -> openSetPlayersWindow());
		registerAction(mainMenuFrame.getStartButtonsPanel().getResultsBtn(), (e) -> openResultsWindow());
		registerAction(mainMenuFrame.getSetGameInfoPanel().getAddTeamButton(), (e) -> addTeamButtonAction());

		// RESULTS BUTTONS
		registerAction(resultsController.getResultsFrame().getResultsPanel().getBackToStartButton(), (e) -> backFromResultsToMainMenu());

		// STATS BUTTONS
		registerAction(statsController.getStatsAppFrame().getMatchInfoPanel().getBackToStartButton(), (e) -> backFromStatsToSetPlayersAction());
		registerActivityPanelItemDoubleClickedAction(statsController.getStatsAppFrame().getActivityPanel());
		
		// SET PLAYERS BUTTONS
		SetPlayersFrame setPlayersFrame = setPlayersController.getSetPlayerFrame();
		AddPlayersPanel addPlayersPanel = setPlayersFrame.getAddPlayersPanel();
		PlayerListPanel playerListPanel = setPlayersFrame.getPlayerListPanel();
		
		registerAction(addPlayersPanel.getAddButton(), (e) -> setPlayersController.addPlayersButtonAction(addPlayersPanel, playerListPanel.getPlayerListModel(), teamStore));		
		registerAction(playerListPanel.getSelectButton(), (e) -> setPlayersController.addSelectButtonAction(playerListPanel.getPlayerListModel(), playerListPanel.getPlayerJlist()));
		registerAction(playerListPanel.getModifyButton(), (e) -> setPlayersController.addModifyButtonAction());
		registerAction(playerListPanel.getRemoveButton(), (e) -> setPlayersController.addRemoveButtonAction(playerListPanel.getPlayerJlist(), playerListPanel.getPlayerListModel(), teamStore));
		registerAction(playerListPanel.getDeSelectButton(), (e) -> setPlayersController.addDeSelectButtonAction(playerListPanel.getPlayerJlist(), playerListPanel.getPlayerListModel()));
		registerAction(playerListPanel.getSwitchButton(), (e) -> setPlayersController.addSwitchButtonAction(playerListPanel.getPlayerJlist(), playerListPanel.getPlayerListModel()));
		registerAction(setPlayersFrame.getBackToStartButton(), e -> backFromSetPlayersToMainMenuAction());
		registerAction(setPlayersFrame.getNextButton(), e -> nextFromSetPlayersToStatsAction());
	}
	
	private void registerActivityPanelItemDoubleClickedAction(ActivityPanel activityPanel) {
		activityPanel.getActivityList().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					modifyScreenEventFrame.createPlayerComboBoxes();
					modifyScreenEventFrame.updateAndShowFrame(statsController.getStatsAppFrame().getActivityPanel().getActivityList().getSelectedValue());
					modifyScreenEventFrame.setVisible(true);
				}
			}
		});
	}

	private void addTeamButtonAction() {
		String teamString = mainMenuFrame.getSetGameInfoPanel().getNewTeamTextField().getText();
		teamStore.addTeam(new Team(teamString, new ArrayList<Player>()));
		mainMenuFrame.getSetGameInfoPanel().addSetGameInfoPanel(teamStore);
		writeXmlFile(TEAM_STORE_FILE_PATH);
	}

	private void backFromResultsToMainMenu() {
		resultsController.getResultsFrame().setVisible(false);
		mainMenuFrame.setVisible(true);
	}

	private void backFromSetPlayersToMainMenuAction() {
		setPlayersController.getSetPlayerFrame().setVisible(false);
		mainMenuFrame.setVisible(true);
	}

	private void nextFromSetPlayersToStatsAction() {
		if (setPlayersController.getSetPlayerFrame().getPlayerListPanel().getSelectedPlayerArray().size() < 2 ) {
			JOptionPane.showMessageDialog(
					null,
					SELECT_ATLEAST_TWO_PLAYERS_ERROR_TEXT,
					ERROR_TEXT,
					JOptionPane.ERROR_MESSAGE);
		}
		else {
			setPlayersController.getSetPlayerFrame().setVisible(false);
			screenEnumProvider.updatePlayerTable(setPlayersController.getSetPlayerFrame().getPlayerListPanel().getSelectedPlayerArray());
			statsController.prepareAndOpenFrame();
			
			// Save XML-file if players added or removed
			if(setPlayersController.getIsPlayerListChanged()) {
				writeXmlFile(TEAM_STORE_FILE_PATH);
			} else {
				// LOG.INFO("No changes, xml not modifies");
			}
			setPlayersController.setIsPlayerListChanged(false);
		}
	}

	private void backFromStatsToSetPlayersAction() {
		statsController.getStatsAppFrame().setVisible(false);
		setPlayersController.getSetPlayerFrame().setVisible(true);
		// Tähän joku resetToDefault-funktio!
		statsController.getStatsAppFrame().setIsOpen(false);
		statsController.getStatsAppFrame().getEnterStatsPanel().setFieldsEnabled(false);
		statsController.getStatsAppFrame().getStatsCourtPanel().setScreenButtonsEnabled(true);
		statsController.getStatsAppFrame().getEnterStatsPanel().setCheckBoxesEnabled(true);
		statsController.getStatsAppFrame().getEnterStatsPanel().emptyTextFields();
		statsController.getStatsAppFrame().getEnterStatsPanel().setAddingElement(false);
	}

	private void readXmlFile(String teamStoreFilePath) {
		// Load xml-file from resources
		
		File source = new File("E:\\team.xml");
		
		// Parse Teams from xml-file
		try {
			Serializer serializer = new Persister();
			this.teamStore = serializer.read(TeamStore.class, source);
			LOG.debug("TeamStore readed from XML");
		} catch (Exception e) {
			LOG.error("Failed to read XML from" + TEAM_STORE_FILE_PATH + ".", e);
		}		
		
	}
	
	private void writeXmlFile(String teamStoreFilePath) {
		// Load xml-file from resources
		File source = new File("E:\\team.xml");
		
		// Write Teams from xml-file
		try {
			Serializer serializer = new Persister();
			serializer.write(teamStore, source);
			LOG.debug("Writing teamStore to XML");
		} catch (Exception e) {
			LOG.error("Failed to write XML from" + TEAM_STORE_FILE_PATH + ".", e);
		}	
	}

	public void hideMainMenuFrame() {

	}

	public MainMenuFrame getMainMenuFrame() {
		return mainMenuFrame;
	}

	private void openSetPlayersWindow() {
		
		teamAgainstName = mainMenuFrame.getSetGameInfoPanel().getTeamAgainstText();
		ownTeam = mainMenuFrame.getSetGameInfoPanel().getOwnTeam();
		
		if (teamAgainstName == ownTeam.getTeamName()) {
			JOptionPane.showMessageDialog(
					null,
					SAME_TEAMS_ERROR_TEXT,
					ERROR_TEXT,
					JOptionPane.ERROR_MESSAGE);
		}
		else if(ownTeam.getTeamName() == null || teamAgainstName == "") {
			JOptionPane.showMessageDialog(
					null,
					SELECT_TEAMS_ERROR_TEXT,
					ERROR_TEXT,
					JOptionPane.ERROR_MESSAGE);
		}
		else {
			gameDate = mainMenuFrame.getSetGameInfoPanel().getDate();
			homeAway = Optional.ofNullable(mainMenuFrame.getSetGameInfoPanel().getHomeAwayPanel().getSelectedHomeAway());
			LOG.trace("Opening setPlayersController with values"
					+ "teamAgainstName: {}, ownTeam: {}, gameDate: {}, homeAway: {}", teamAgainstName, ownTeam, gameDate, homeAway);
			setPlayersController.prepareAndOpenFrame(ownTeam);
			
			teamStore.setLastOwnTeam(ownTeam.getTeamName());
			writeXmlFile(TEAM_STORE_FILE_PATH);
			
			// When setPlayers is opened, teamNames, gameDate, and HomeAway can be stored to statsController
			// -> any of these cannot be modified from setPlayers
			statsController.setTeamNamesAndDate(ownTeam.getTeamName(), teamAgainstName, gameDate, homeAway);
			mainMenuFrame.setVisible(false);
		}
	}

	private void openResultsWindow() {
		mainMenuFrame.setVisible(false);
		resultsController.prepareAndOpenFrame(teamStore);
	}

	public TeamStore getTeamStore() {
		return teamStore;
	}

	public void setTeamStore(TeamStore teamStore) {
		this.teamStore = teamStore;
	}
	
}
