package fi.statsapp.basketstats.main_menu.ui;

import javax.swing.JFrame;

import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.business.player.TeamStore;
import net.miginfocom.swing.MigLayout;

@Component
public class MainMenuFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private SetGameInfoPanel setGameInfoPanel;
	private StartButtonsPanel startButtonsPanel;
	
	public MainMenuFrame(SetGameInfoPanel setGameInfoPanel, StartButtonsPanel startButtonsPanel) {
		this.setGameInfoPanel = setGameInfoPanel;
		this.startButtonsPanel = startButtonsPanel;
		initFrame();
		initComponents();

	}

	private void initFrame() {
		setLayout(new MigLayout("", "[]", "[][]"));
		setTitle("StatsAPP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		initComponents();
		pack();
		setLocationRelativeTo(null);
		
	}

	private void initComponents() {
		add(this.setGameInfoPanel, "wrap");
		add(this.startButtonsPanel, "wrap, pushx, alignx center");		
	}

	public SetGameInfoPanel getSetGameInfoPanel() {
		return setGameInfoPanel;
	}

	public StartButtonsPanel getStartButtonsPanel() {
		return startButtonsPanel;
	}
	
	public void addSetGameInfoPanel(TeamStore teamStore) {
		setGameInfoPanel.addSetGameInfoPanel(teamStore);
		pack();
		setLocationRelativeTo(null);
	}
	
}
