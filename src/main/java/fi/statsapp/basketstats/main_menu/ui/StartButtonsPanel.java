package fi.statsapp.basketstats.main_menu.ui;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import net.miginfocom.swing.MigLayout;

@Component
public class StartButtonsPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static final String START_MATCH_BUTTON_TXT = "Start Match";
	private static final String SEARCH_RESULTS_BUTTON_TXT = "Search Results";

	private JButton setPlayersBtn;
	private JButton resultsBtn;
	
	public StartButtonsPanel() {
		initComponents();
	}
	
	private void initComponents() {
		setLayout(new MigLayout());
		
		setPlayersBtn = new JButton(START_MATCH_BUTTON_TXT);
		resultsBtn = new JButton(SEARCH_RESULTS_BUTTON_TXT);		

		add(setPlayersBtn, "alignx center, wrap");
		add(resultsBtn, "alignx center");
	}

	public JButton getSetPlayersPanelBtn() {
		return setPlayersBtn;
	}
	
	public JButton getResultsBtn() {
		return resultsBtn;
	}
}
