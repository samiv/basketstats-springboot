package fi.statsapp.basketstats.main_menu.ui;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.springframework.stereotype.Component;

import fi.statsapp.basketstats.DateLabelFormatter;
import fi.statsapp.basketstats.business.player.HomeAwayRadioButtonPanel;
import fi.statsapp.basketstats.business.player.Team;
import fi.statsapp.basketstats.business.player.TeamStore;
import net.miginfocom.swing.MigLayout;

@Component
public class SetGameInfoPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final String OWN_TEAM_LBL_TEXT = "Own Team: ";
	private static final String TEAM_AGAINST_LBL_TXT = "Team Against: ";
	private static final String DATE_LBL_TXT = "Game Date: ";
	private static final String ADD_TEAM_BUTTON_TEXT = "Add Team";

	private JButton addTeamButton;
	private JTextField newTeamTextField;

	private JComboBox<Team> ownTeamCB;
	private JComboBox<Team> teamAgainstCB;
	
	private HomeAwayRadioButtonPanel homeAwayPanel;

	private JDatePickerImpl datePicker;

	public SetGameInfoPanel() {
		setLayout(new MigLayout("","[][][]","[][][]13px[]"));
	}

	public void addSetGameInfoPanel(TeamStore teamStore) {
		removeAll();
		
		// Home/Away -radioButton
		homeAwayPanel = new HomeAwayRadioButtonPanel();
		add(homeAwayPanel, "wrap");	
		
		addTeamComboBoxes(teamStore);

		addDatePicker();

		revalidate();
		repaint();
	}

	private void addTeamComboBoxes(TeamStore teamStore) {
		addTeamButton = new JButton(ADD_TEAM_BUTTON_TEXT);
		newTeamTextField = new JTextField();
		newTeamTextField.setColumns(10);
		ownTeamCB = new JComboBox<Team>();
		teamAgainstCB = new JComboBox<Team>();
		
		List<Team> teamList = teamStore.getTeamList();
		for(Team team : teamList) {
			ownTeamCB.addItem(team);
			teamAgainstCB.addItem(team);
		}

		String lastOwnTeamString = teamStore.getLastOwnTeam();
		ownTeamCB.insertItemAt(new Team(null, null), 0);	
		if (lastOwnTeamString != null) {
			ownTeamCB.setSelectedItem(teamStore.getTeam(lastOwnTeamString));
		}
		else {
			ownTeamCB.setSelectedIndex(0);
		}
		teamAgainstCB.insertItemAt(new Team(null, null), 0);
		teamAgainstCB.setSelectedIndex(0);

		// CheckBoxes
		add(new JLabel(OWN_TEAM_LBL_TEXT));
		add(ownTeamCB);
		add(newTeamTextField, "wrap");	
		add(new JLabel(TEAM_AGAINST_LBL_TXT));
		add(teamAgainstCB);
		add(addTeamButton, "wrap");
	}

	private void addDatePicker() {

		add(new JLabel(DATE_LBL_TXT));
		UtilDateModel model = new UtilDateModel();
		LocalDate now = LocalDate.now();
		// TODO: Selvitä miksi -1, toimiiko tammikuussa. Entä joulukuussa
		model.setDate(now.getYear(), now.getMonthValue()-1, now.getDayOfMonth());
		model.setSelected(true);
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());		 
		add(datePicker, "wrap");

	}

	public HomeAwayRadioButtonPanel getHomeAwayPanel() {
		return homeAwayPanel;
	}

	public String getOwnTeamText() {
		return ownTeamCB.getSelectedItem().toString();
	}
	
	public Team getOwnTeam() {
		return ownTeamCB.getItemAt(ownTeamCB.getSelectedIndex());
	}

	public String getTeamAgainstText() {
		return teamAgainstCB.getSelectedItem().toString();
	}
	
	public Team getTeamAgainst() {
		return teamAgainstCB.getItemAt(teamAgainstCB.getSelectedIndex());
	}

	public JButton getAddTeamButton() {
		return addTeamButton;
	}

	public JTextField getNewTeamTextField() {
		return newTeamTextField;
	}

	public Date getDate() {
		return (Date) datePicker.getModel().getValue();
	}

}
