package fi.statsapp.basketstats;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import fi.statsapp.basketstats.business.repository.ScreenEventRepository;
import fi.statsapp.basketstats.business.ui.results.ResultsApp;
import net.miginfocom.swing.MigLayout;

@SpringBootApplication
@EnableAutoConfiguration
public class BasketStatBootApplication extends JFrame {

	@Autowired
	ScreenEventRepository cr;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String START_SCREEN_TITLE = "StatsApp";
	private static final int APPSIZE_X = 1000;
	private static final int APPSIZE_Y = 610;
	private JTextField homeTextField;
	private JTextField questTextField;
	private static final String HOME_PANEL_TEXT = "HOME TEAM NAME";
	private static final String GUEST_PANEL_TEXT = "GUEST TEAM NAME";

	public BasketStatBootApplication() {
		initUI();
	}

	private void initUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		setResizable(false);
		setTitle(START_SCREEN_TITLE);
		setContentPane(CreateStartScreenPanel(this));
		pack();
		setLocationRelativeTo(null);
		
//		cr.save(new ScreenEvent(ScreenPosition.ELBOW_LEFT,
//				1,
//				"etu",
//				2,
//				"atu",
//				Direction.CENTER,
//				ScreenDefenceStyle.GAP_UNDER,
//				ScreenEnding.PASS,
//				ShotClockTime.EARLY));
//
//
//		List<ScreenEvent> cm = cr.findAll();
//
//
//		for (ScreenEvent c : cm) {
//			System.out.println(c);
//		}

	}

	public static void main(String[] args) {		

		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(BasketStatBootApplication.class)
				.headless(false).run(args);

		EventQueue.invokeLater(() -> {
			BasketStatBootApplication ex = ctx.getBean(BasketStatBootApplication.class);
			ex.setVisible(true);
		});
	}

//	public static void saveEvent(ScreenEvent se) {
//		cr.save(se);
//	}

	public JPanel CreateStartScreenPanel(JFrame frame) {
		JPanel panel = new JPanel();
		panel.setLayout(new MigLayout());
		JPanel textPanel = new JPanel(new MigLayout("insets 0 0 0 0","[]15px[180px]","[30px]8px[30px]"));
		JPanel buttonPanel = new JPanel(new MigLayout("insets 0 100 10 10", "", "[]10px[]"));
		//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));


		JLabel homePanel = new JLabel(HOME_PANEL_TEXT);
		textPanel.add(homePanel, "cell 0 0, alignx trailing");		

		homeTextField = new JTextField();
		homeTextField.setColumns(12);
		homeTextField.setHorizontalAlignment(JTextField.LEFT);
		textPanel.add(homeTextField, "cell 1 0, growy, growx, wrap");

		JLabel questPanel = new JLabel(GUEST_PANEL_TEXT);
		textPanel.add(questPanel, "cell 0 1, alignx trailing");

		questTextField = new JTextField();
		questTextField.setColumns(12);
		questTextField.setHorizontalAlignment(JTextField.LEFT);
		/// ADD QUESTEXTFIELD
		textPanel.add(questTextField, "cell 1 1, growy, growx, wrap");

		/// ADD START BUTTON
		JButton btnStartMatch = new JButton("Start match");
		final boolean validatorsCorrect1 = true;
		btnStartMatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(validatorsCorrect1) {

//					SetPlayersFrame setPlayersView = new SetPlayersFrame(/*questTextField.getText(), homeTextField.getText()*/);
//					setPlayersView.setVisible(true);
					frame.setVisible(false);

				}
			}
		});
		buttonPanel.add(btnStartMatch, "cell 0 0, wrap, growx");

		/// ADD SEARCH BUTTON
		JButton btnSearchResults = new JButton("Search results");
		final boolean validatorsCorrect = true;
		btnSearchResults.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(validatorsCorrect) {
					ResultsApp resultsView = new ResultsApp();
					resultsView.getResultsFrame().setVisible(true);
					frame.setVisible(false);
				}
			}
		});
		buttonPanel.add(btnSearchResults, "cell 0 1, growx");
		panel.add(textPanel, "wrap");
		panel.add(buttonPanel);
		return panel;
	}

	public static int getAppSizeX() {
		return APPSIZE_X;
	}

	public static int getAppSizeY() {
		return APPSIZE_Y;
	}
}
