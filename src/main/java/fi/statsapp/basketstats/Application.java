package fi.statsapp.basketstats;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import fi.statsapp.basketstats.main_menu.controller.MainMenuController;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
//		LookAndFeelUtils.setWindowsLookAndFeel();
		ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class).headless(false).run(args);
	    MainMenuController mainMenuController = context.getBean(MainMenuController.class);
	    mainMenuController.prepareAndOpenFrame();
	}

}
