package fi.statsapp.basketstats;

import javax.swing.JButton;
import javax.swing.JFrame;

public abstract class AbstractAppFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	protected JButton backButton;

	private final static int APPSIZE_X = 1000;
	private final static int APPSIZE_Y = 610;

	public abstract void initialize();


	protected static int getAppSizeX() {
		return APPSIZE_X;
	}

	protected static int getAppSizeY() {
		return APPSIZE_Y;
	}

}

