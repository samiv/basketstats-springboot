package fi.statsapp.basketstats.system.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "fi.statsapp.basketstats.business")
@EnableJpaRepositories(basePackages = "fi.statsapp.basketstats.business.repository")
public class ApplicationConfiguration {
}
