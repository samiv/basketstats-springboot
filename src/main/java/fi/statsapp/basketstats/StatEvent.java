package fi.statsapp.basketstats;

public interface StatEvent {
	
	String toRecentListString();
	
	String toResultsListString();

}
